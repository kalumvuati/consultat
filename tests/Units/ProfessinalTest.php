<?php

namespace App\Tests\Units;

use App\Entity\Address;
use App\Entity\Contact;
use App\Entity\Diploma;
use App\Entity\Event;
use App\Entity\Experience;
use App\Entity\Plan;
use App\Entity\Professional;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProfessinalTest extends KernelTestCase
{

    /**
     * @dataProvider providerIsTrue
     */
    public function testIsTrue(Professional $Professional): void
    {
        $today = new \DateTime();
        $this->assertEquals('12345678900067', $Professional->getSiret());
        $this->assertEquals($today->format('d'), $Professional->getCreatedAt()->format('d'));
        $this->assertEquals('googleId', $Professional->getGoogleId());
        $this->assertInstanceOf(Address::class, $Professional->getAddresses()->first());
        $this->assertInstanceOf(Event::class, $Professional->getEvents()->first());
        $this->assertInstanceOf(Contact::class, $Professional->getContacts()->first());
        $this->assertInstanceOf(Plan::class, $Professional->getPlan());
        $this->assertInstanceOf(Experience::class, $Professional->getExperiences()->first());
        $this->assertInstanceOf(Diploma::class, $Professional->getDiplomas()->first());
    }

    /**
     * @dataProvider providerIsTrue
     */
    public function testIsFalse(Professional $Professional): void
    {
        $today = new \DateTime('+1 day');
        $this->assertNotEquals('12345678900068', $Professional->getSiret());
        $this->assertNotEquals($today->format('d'), $Professional->getCreatedAt()->format('d'));
        $this->assertNotEquals('false', $Professional->getGoogleId());
        $this->assertNotInstanceOf(Experience::class, $Professional->getAddresses()->first());
        $this->assertNotInstanceOf(Contact::class, $Professional->getEvents()->first());
        $this->assertNotInstanceOf(Event::class, $Professional->getContacts()->first());
        $this->assertNotInstanceOf(Contact::class, $Professional->getPlan());
        $this->assertNotInstanceOf(Event::class, $Professional->getExperiences()->first());
        $this->assertNotInstanceOf(Address::class, $Professional->getDiplomas()->first());
    }

    /**
     * @dataProvider providerIsEmpty
     */
    public function testIsEmpty(Professional $Professional): void
    {
        $this->assertEmpty($Professional->getSiret());
        $this->assertEmpty($Professional->getGoogleId());
        $this->assertEmpty($Professional->getAddresses());
        $this->assertEmpty($Professional->getEvents());
        $this->assertEmpty($Professional->getContacts());
        $this->assertEmpty($Professional->getPlan());
        $this->assertEmpty($Professional->getExperiences());
        $this->assertEmpty($Professional->getDiplomas());
    }

    /**
     * @dataProvider providerIsTrue
     */
    public function testSiretValid(Professional $professional): void
    {
        self::bootKernel();
        //TODO - to seek an other way to do that (By dependency injection)
        $errors = self::$container->get('validator')->validate($professional);
        $this->assertCount(0, $errors);
    }

    /**
     * @dataProvider providerBadSiret
     */
    public function testSiretNotValid(Professional $professional): void
    {
        self::bootKernel();
        //TODO - to seek an other way to do that (By dependency injection)
        $errors = self::$container->get('validator')->validate($professional);
        $this->assertCount(1, $errors);
    }

    public function providerBadSiret(): array
    {
        $Professionals = [];
        for ($i = 0; $i < 5; ++$i) {
            $Professional = new Professional();
            $today = new \DateTime();
            $Professional->setSiret('123456789000674')
                ->setCreatedAt($today)
                ->setGoogleId('googleId')
                ->addAddress(new Address())
                ->addEvent(new Event())
                ->addContact(new Contact())
                ->addExperience(new Experience())
                ->setPlan(new Plan())
                ->addDiploma(new Diploma());
            $Professionals[] = [$Professional];
        }

        return $Professionals;
    }

    public function providerIsTrue(): array
    {
        $Professionals = [];
        for ($i = 0; $i < 5; ++$i) {
            $Professional = new Professional();
            $today = new \DateTime();
            $Professional->setSiret('12345678900067')
                ->setCreatedAt($today)
                ->setGoogleId('googleId')
                ->addAddress(new Address())
                ->addEvent(new Event())
                ->addContact(new Contact())
                ->addExperience(new Experience())
                ->setPlan(new Plan())
                ->addDiploma(new Diploma());
            $Professionals[] = [$Professional];
        }

        return $Professionals;
    }

    public function providerIsEmpty(): array
    {
        $Professionals = [];
        for ($i = 0; $i < 5; ++$i) {
            $Professional = new Professional();
            $today = new \DateTime();
            $Professional->setSiret('')
                ->setCreatedAt($today)
                ->setGoogleId('');
            $Professionals[] = [$Professional];
        }

        return $Professionals;
    }

}
