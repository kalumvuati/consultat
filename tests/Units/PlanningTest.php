<?php

namespace App\Tests\Units;

use App\Entity\Event;
use App\Entity\Planning;
use App\Entity\Professional;
use PHPUnit\Framework\TestCase;

class PlanningTest extends TestCase
{
    /**
     * @dataProvider providerIsTrue
     */
    public function testIsTrue(Planning $Planning): void
    {
        //Planning for morning
        $startPM = new \DateTime();
        $startAM = new \DateTime();
        $startPM->setTime(8, 0);
        $startAM->setTime(12, 0);

        //Planning for afternoon
        $endPM = new \DateTime();
        $endAM = new \DateTime();
        $endPM->setTime(13, 30);
        $endAM->setTime(17, 0);

        $this->assertEquals($startPM->format('h'), $Planning->getStartPmAt()->format('h'));
        $this->assertEquals($endPM->format('h'), $Planning->getEndPmAt()->format('h'));
        $this->assertEquals($startAM->format('H:i'), $Planning->getStartAmAt()->format('H:i'));
        $this->assertEquals($endAM->format('h'), $Planning->getEndAmAt()->format('h'));
        $this->assertEquals(Planning::DAYS['Monday'], $Planning->getDay());
        $this->assertInstanceOf(Professional::class, $Planning->getProfessional());

        //check different between start and end planning
        $this->assertGreaterThanOrEqual($Planning->getEndPmAt()->format('h'),
            $Planning->getStartPmAt()->format('h'));
        $this->assertGreaterThanOrEqual($Planning->getStartAmAt()->format('H'),
            $Planning->getEndPmAt()->format('H'));
        $this->assertGreaterThanOrEqual($Planning->getEndAmAt()->format('h'),
            $Planning->getStartAmAt()->format('h'));
    }

    /**
     * @dataProvider providerIsTrue
     */
    public function testIsFalse(Planning $Planning): void
    {
        //Planning for morning
        $startPM = new \DateTime();
        $startAM = new \DateTime();
        $startPM->setTime(17, 0);
        $startAM->setTime(13, 0);

        //Planning for afternoon
        $endPM = new \DateTime();
        $endAM = new \DateTime();
        $endPM->setTime(12, 20);
        $endAM->setTime(8, 0);

        $this->assertNotEquals($startPM->format('h'), $Planning->getStartPmAt()->format('h'));
        $this->assertNotEquals($endPM->format('h'), $Planning->getEndPmAt()->format('h'));
        $this->assertNotEquals($startAM->format('H:i'), $Planning->getStartAmAt()->format('H:i'));
        $this->assertNotEquals($endAM->format('h'), $Planning->getEndAmAt()->format('h'));
        $this->assertNotEquals(Planning::DAYS['Tuesday'], $Planning->getDay());
        $this->assertNotInstanceOf(Event::class, $Planning->getProfessional());

        //check different between start and end planning
        $this->assertGreaterThanOrEqual($Planning->getEndPmAt()->format('h'),
            $Planning->getStartPmAt()->format('h'));
        $this->assertGreaterThanOrEqual($Planning->getStartAmAt()->format('H'),
            $Planning->getEndPmAt()->format('H'));
        $this->assertGreaterThanOrEqual($Planning->getEndAmAt()->format('h'),
            $Planning->getStartAmAt()->format('h'));
    }

    /**
     * @dataProvider providerIsEmpty
     */
    public function testIsEmpty(Planning $Planning): void
    {
        $this->assertEmpty($Planning->getStartPmAt());
        $this->assertEmpty($Planning->getEndPmAt());
        $this->assertEmpty($Planning->getStartAmAt());
        $this->assertEmpty($Planning->getEndAmAt());
        $this->assertEmpty($Planning->getDay());
        $this->assertEmpty($Planning->getProfessional());
    }

    public function providerIsTrue(): array
    {
        $Plannings = [];

        //Planning for morning
        $startPM = new \DateTime();
        $startAM = new \DateTime();
        $startPM->setTime(8, 0);
        $startAM->setTime(12, 0);

        //Planning for afternoon
        $endPM = new \DateTime();
        $endAM = new \DateTime();
        $endPM->setTime(13, 30);
        $endAM->setTime(17, 0);

        for ($i = 0; $i < 5; ++$i) {
            $Planning = (new Planning())
                ->setStartPmAt($startPM)
                ->setEndPmAt($endPM)
                ->setStartAmAt($startAM)
                ->setEndAmAt($endAM)
                ->setDay(Planning::DAYS['Monday'])
                ->setProfessional(new Professional());
            $Plannings[] = [$Planning];
        }

        return $Plannings;
    }

    public function providerIsEmpty(): array
    {
        $Plannings = [];
        for ($i = 0; $i < 5; ++$i) {
            $Planning = new Planning();
            $Plannings[] = [$Planning];
        }

        return $Plannings;
    }
}
