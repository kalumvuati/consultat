<?php

namespace App\Tests\Units;

use App\Entity\Event;
use App\Entity\Interval;
use App\Entity\Plan;
use App\Entity\Professional;
use PHPUnit\Framework\TestCase;

class PlanTest extends TestCase
{
    /**
     * @dataProvider providerIsTrue
     */
    public function testIsTrue(Plan $Plan): void
    {
        $today = new \DateTime();
        $this->assertEquals('title', $Plan->getTitle());
        $this->assertEquals($today->format('d'), $Plan->getCreatedAt()->format('d'));
        $this->assertEquals(1.2, $Plan->getPrice());
        $this->assertEquals('eur', $Plan->getCurrency());
        $this->assertEquals('description', $Plan->getDescription());
        $this->assertInstanceOf(Interval::class, $Plan->getIntervale());
        $this->assertInstanceOf(Professional::class, $Plan->getProfessionnals()->first());
    }

    /**
     * @dataProvider providerIsTrue
     */
    public function testIsFalse(Plan $Plan): void
    {
        $today = new \DateTime('+1 day');
        $this->assertNotEquals('false', $Plan->getTitle());
        $this->assertNotEquals($today->format('d'), $Plan->getCreatedAt()->format('d'));
        $this->assertNotEquals(1.3, $Plan->getPrice());
        $this->assertNotEquals('dollar', $Plan->getCurrency());
        $this->assertNotEquals('false', $Plan->getDescription());
        $this->assertNotInstanceOf(Professional::class, $Plan->getIntervale());
        $this->assertNotInstanceOf(Event::class, $Plan->getProfessionnals()->first());
    }

    /**
     * @dataProvider providerIsEmpty
     */
    public function testIsEmpty(Plan $Plan): void
    {
        $this->assertEmpty($Plan->getTitle());
        $this->assertEmpty($Plan->getPrice());
        $this->assertEmpty($Plan->getCurrency());
        $this->assertEmpty($Plan->getDescription());
        $this->assertEmpty($Plan->getIntervale());
        $this->assertEmpty($Plan->getProfessionnals()->first());
    }

    public function providerIsTrue(): array
    {
        $Plans = [];
        $today = new \DateTime();
        for ($i = 0; $i < 5; ++$i) {
            $Plan = new Plan();
            $Plan->setTitle('title')
                ->setCreatedAt($today)
                ->setPrice(1.2)
                ->setCurrency('eur')
                ->setDescription('description')
                ->setIntervale(new Interval())
                ->addProfessionnal(new Professional());
            $Plans[] = [$Plan];
        }

        return $Plans;
    }

    public function providerIsEmpty(): array
    {
        $Plans = [];
        for ($i = 0; $i < 5; ++$i) {
            $Plan = new Plan();
            $Plans[] = [$Plan];
        }

        return $Plans;
    }
}
