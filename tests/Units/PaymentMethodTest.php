<?php

namespace App\Tests\Units;

use App\Entity\Event;
use App\Entity\PaymentMethod;
use App\Entity\Professional;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PaymentMethodTest extends KernelTestCase
{
    /**
     * @dataProvider providerIsTrue
     */
    public function testIsTrue(PaymentMethod $paymentMethod): void
    {
        $this->assertEquals('John DOE', $paymentMethod->getCardNames());
        $this->assertEquals('4242424242424242', $paymentMethod->getCardNumber());
        $this->assertEquals(12, $paymentMethod->getCardMonth());
        $this->assertEquals(24, $paymentMethod->getCardYear());
        $this->assertEquals(454, $paymentMethod->getCardCvc());
        $this->assertInstanceOf(Professional::class, $paymentMethod->getProfessional());
    }

    /**
     * @dataProvider providerIsTrue
     */
    public function testIsFalse(PaymentMethod $paymentMethod): void
    {
        $this->assertNotEquals('John DOA', $paymentMethod->getCardNames());
        $this->assertNotEquals('4242424242424241', $paymentMethod->getCardNumber());
        $this->assertNotEquals('02', $paymentMethod->getCardMonth());
        $this->assertNotEquals('23', $paymentMethod->getCardYear());
        $this->assertNotEquals('451', $paymentMethod->getCardCvc());
        $this->assertNotInstanceOf(Event::class, $paymentMethod->getProfessional());
    }

    /**
     * @dataProvider providerIsEmpty
     */
    public function testIsEmpty(PaymentMethod $paymentMethod): void
    {
        $this->assertEmpty($paymentMethod->getCardNames());
        $this->assertEmpty($paymentMethod->getCardNumber());
        $this->assertEmpty($paymentMethod->getCardMonth());
        $this->assertEmpty($paymentMethod->getCardYear());
        $this->assertEmpty($paymentMethod->getCardCvc());
        $this->assertEmpty($paymentMethod->getProfessional());
    }

    /**
     * @dataProvider providerIsTrue
     */
    public function testCardValide(PaymentMethod $paymentMethod): void
    {
        self::bootKernel();
        //TODO - to seek an other way to do that (By dependency injection)
        $errors = self::$container->get('validator')->validate($paymentMethod);
        $this->assertCount(0, $errors);
    }

    /**
     * @dataProvider providerIsFalse
     */
    public function testCardNotValide(PaymentMethod $paymentMethod): void
    {
        self::bootKernel();
        //TODO - to seek an other way to do that (By dependency injection)
        $errors = self::$container->get('validator')->validate($paymentMethod);
        $this->assertCount(5, $errors);
    }

    public function providerIsTrue(): array
    {
        $paymentMethods = [];
        for ($i = 0; $i < 5; ++$i) {
            $paymentMethod = new PaymentMethod();
            $paymentMethod->setCardNames('John DOE')
                ->setCardNumber('4242424242424242')
                ->setCardMonth(12)
                ->setCardYear(24)
                ->setCardCvc(454)
                ->setProfessional(new Professional());
            $paymentMethods[] = [$paymentMethod];
        }

        return $paymentMethods;
    }

    public function providerIsFalse(): array
    {
        $paymentMethods = [];
        for ($i = 0; $i < 5; ++$i) {
            $paymentMethod = new PaymentMethod();
            $paymentMethod->setCardNames('John')
                ->setCardNumber('42424242424242424')
                ->setCardMonth(13)
                ->setCardYear(19)
                ->setCardCvc(45)
                ->setProfessional(new Professional());
            $paymentMethods[] = [$paymentMethod];
        }

        return $paymentMethods;
    }

    public function providerIsEmpty(): array
    {
        $paymentMethods = [];
        for ($i = 0; $i < 5; ++$i) {
            $paymentMethod = new PaymentMethod();
            $paymentMethods[] = [$paymentMethod];
        }

        return $paymentMethods;
    }
}
