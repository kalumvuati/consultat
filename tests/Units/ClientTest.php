<?php

namespace App\Tests\Units;

use App\Entity\Address;
use App\Entity\Client;
use App\Entity\Event;
use App\Entity\Professional;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ClientTest extends KernelTestCase
{
    /**
     * @dataProvider providerIsTrue
     */
    public function testIsTrue(Client $Client): void
    {
        $today = new \DateTime();
        $this->assertEquals('name', $Client->getName());
        $this->assertEquals('lastname', $Client->getLastname());
        $this->assertEquals($today->format('d.m.Y'), $Client->getBirthday()->format('d.m.Y'));
        $this->assertEquals(true, $Client->getSex());
        $this->assertEquals('075369200', $Client->getPhone());
        $this->assertEquals($today->format('d'), $Client->getCreatedAt()->format('d'));
        $this->assertInstanceOf(User::class, $Client->getUser());
        $this->assertInstanceOf(Address::class, $Client->getAddress());
        $this->assertInstanceOf(Event::class, $Client->getEvents()->first());
    }

    /**
     * @dataProvider providerIsTrue
     */
    public function testIsFalse(Client $Client): void
    {
        $today = new \DateTime('+1 day');
        $this->assertNotEquals('false', $Client->getName());
        $this->assertNotEquals('false', $Client->getLastname());
        $this->assertNotEquals($today->format('d.m.Y'), $Client->getBirthday()->format('d.m.Y'));
        $this->assertNotEquals(false, $Client->getSex());
        $this->assertNotEquals('075369202', $Client->getPhone());
        $this->assertNotEquals($today->format('d'), $Client->getCreatedAt()->format('d'));
        $this->assertNotInstanceOf(Event::class, $Client->getUser());
        $this->assertNotInstanceOf(User::class, $Client->getAddress());
        $this->assertNotInstanceOf(Professional::class, $Client->getEvents()->first());
    }

    /**
     * @dataProvider providerIsEmpty
     */
    public function testIsEmpty(Client $Client): void
    {
        $this->assertEmpty($Client->getName());
        $this->assertEmpty($Client->getLastname());
        $this->assertEmpty($Client->getBirthday());
        $this->assertEmpty($Client->getSex());
        $this->assertEmpty($Client->getPhone());
        $this->assertEmpty($Client->getUser());
        $this->assertEmpty($Client->getAddress());
        $this->assertEmpty($Client->getEvents()->first());
    }

    public function providerIsTrue(): array
    {
        $Clients = [];
        $today = new \DateTime();
        for ($i = 0; $i < 5; ++$i) {
            $Client = new Client();
            $Client->setName('name')
                ->setLastname('lastname')
                ->setBirthday($today)
                ->setSex(true)
                ->setPhone('075369200')
                ->setCreatedAt($today)
                ->setUser(new User())
                ->setAddress(new Address())
                ->addEvent(new Event());
            $Clients[] = [$Client];
        }

        return $Clients;
    }

    public function providerIsEmpty(): array
    {
        $Clients = [];
        for ($i = 0; $i < 5; ++$i) {
            $Client = new Client();
            $Clients[] = [$Client];
        }

        return $Clients;
    }
}
