<?php

namespace App\Tests\Units;

use App\Entity\Admin;
use App\Entity\Client;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    /**
     * @dataProvider providerIsTrue
     */
    public function testIsTrue(User $user): void
    {
        $this->assertEquals('email@email.com', $user->getEmail());
        $this->assertEquals($user->getPassword(), 'password');
        $this->assertEquals($user->isVerified(), true);
    }

    /**
     * @dataProvider providerIsTrue
     */
    public function testIsFalse(User $user): void
    {
        $this->assertNotEquals($user->getEmail(), 'false');
        $this->assertNotEquals($user->getPassword(), 'passw');
        $this->assertNotEquals($user->isVerified(), false);
    }

    /**
     * @dataProvider providerIsEmpty
     */
    public function testIsEmpty(User $user): void
    {
        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getClient());
        $this->assertEmpty($user->getAdministrator());
    }

    public function providerIsTrue(): array
    {
        $users = [];
        for ($i = 0; $i < 5; ++$i) {
            $user = new User();
            $user->setEmail('email@email.com')
                ->setPassword('password')
                ->setClient(new Client())
                ->setIsVerified(true)
                ->setAdministrator(new Admin());
            $users[] = [$user];
        }

        return $users;
    }

    public function providerIsEmpty(): array
    {
        $users = [];
        for ($i = 0; $i < 5; ++$i) {
            $user = new User();
            $user->setPassword('');
            $user->setRoles([]);
            $users[] = [$user];
        }

        return $users;
    }
}
