<?php

namespace App\Tests\Units;

use App\Entity\Event;
use App\Entity\Experience;
use App\Entity\Professional;
use PHPUnit\Framework\TestCase;

class ExperienceTest extends TestCase
{
    /**
     * @dataProvider providerIsTrue
     */
    public function testIsTrue(Experience $Experience): void
    {
        $start = new \DateTime();
        $end = new \DateTime('+1 day');
        $this->assertEquals($start->format('d'), $Experience->getStartAt()->format('d'));
        $this->assertEquals($end->format('d'), $Experience->getEndAt()->format('d'));
        $this->assertGreaterThan($Experience->getStartAt()->format('d-m'), $Experience->getEndAt()->format('d-m'));
        $this->assertEquals('description', $Experience->getDescription());
        $this->assertEquals('location', $Experience->getLocation());
        $this->assertInstanceOf(Professional::class, $Experience->getProfessionals()->first());
    }

    /**
     * @dataProvider providerIsTrue
     */
    public function testIsFalse(Experience $Experience): void
    {
        $start = new \DateTime();
        $end = new \DateTime('+1 day');
        $this->assertNotEquals($end->format('d'), $Experience->getStartAt()->format('d'));
        $this->assertNotEquals($start->format('d'), $Experience->getEndAt()->format('d'));
        $this->assertLessThan($Experience->getEndAt()->format('d-m'), $Experience->getStartAt()->format('d-m'));
        $this->assertNotEquals('false', $Experience->getDescription());
        $this->assertNotEquals('false', $Experience->getLocation());
        $this->assertNotInstanceOf(Event::class, $Experience->getProfessionals()->first());
    }

    /**
     * @dataProvider providerIsEmpty
     */
    public function testIsEmpty(Experience $Experience): void
    {
        $this->assertEmpty($Experience->getStartAt());
        $this->assertEmpty($Experience->getEndAt());
        $this->assertEmpty($Experience->getDescription());
        $this->assertEmpty($Experience->getLocation());
        $this->assertEmpty($Experience->getProfessionals()->first());
    }

    public function providerIsTrue(): array
    {
        $Experiences = [];
        $start = new \DateTime();
        $end = new \DateTime('+1 day');
        for ($i = 0; $i < 5; ++$i) {
            $Experience = (new Experience())
            ->setStartAt($start)
            ->setEndAt($end)
            ->setDescription('description')
            ->setLocation('location')
            ->addProfessional(new Professional());
            $Experiences[] = [$Experience];
        }

        return $Experiences;
    }

    public function providerIsEmpty(): array
    {
        $Experiences = [];
        for ($i = 0; $i < 5; ++$i) {
            $Experience = new Experience();
            $Experiences[] = [$Experience];
        }

        return $Experiences;
    }
}
