<?php

namespace App\Tests\Units;

use App\Entity\Client;
use App\Entity\Event;
use App\Entity\Professional;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class EventTest extends TestCase
{
    /**
     * @dataProvider providerIsTrue
     */
    public function testIsTrue(Event $Event): void
    {
        $start = new \DateTime();
        $end = new \DateTime('+1 day');
        $this->assertEquals('title', $Event->getTitle());
        $this->assertEquals($start->format('d'), $Event->getStartAt()->format('d'));
        $this->assertEquals($end->format('d'), $Event->getEndAt()->format('d'));
        $this->assertEquals(true, $Event->getIsAborted());
        $this->assertEquals('description', $Event->getDescription());
        $this->assertInstanceOf(Client::class, $Event->getClient());
        $this->assertInstanceOf(Professional::class, $Event->getProfessional());
    }

    /**
     * @dataProvider providerIsTrue
     */
    public function testIsFalse(Event $Event): void
    {
        $start = new \DateTime();
        $end = new \DateTime('+1 day');
        $this->assertNotEquals('false', $Event->getTitle());
        $this->assertNotEquals($end->format('d'), $Event->getStartAt()->format('d'));
        $this->assertNotEquals($start->format('d'), $Event->getEndAt()->format('d'));
        $this->assertNotEquals(false, $Event->getIsAborted());
        $this->assertNotEquals('false', $Event->getDescription());
        $this->assertNotInstanceOf(Event::class, $Event->getClient());
        $this->assertNotInstanceOf(User::class, $Event->getProfessional());
    }

    /**
     * @dataProvider providerIsEmpty
     */
    public function testIsEmpty(Event $Event): void
    {
        $this->assertEmpty($Event->getTitle());
        $this->assertEmpty($Event->getStartAt());
        $this->assertEmpty($Event->getEndAt());
        $this->assertEmpty($Event->getIsAborted());
        $this->assertEmpty($Event->getDescription());
        $this->assertEmpty($Event->getClient());
        $this->assertEmpty($Event->getProfessional());
    }

    public function providerIsTrue(): array
    {
        $Events = [];
        $start = new \DateTime();
        $end = new \DateTime('+1 day');
        for ($i = 0; $i < 5; ++$i) {
            $Event = new Event();
            $Event->setTitle('title')
                ->setStartAt($start)
                ->setEndAt($end)
                ->setIsAborted(true)
                ->setDescription('description')
                ->setClient(new Client())
                ->setProfessional(new Professional());
            $Events[] = [$Event];
        }

        return $Events;
    }

    public function providerIsEmpty(): array
    {
        $Events = [];
        for ($i = 0; $i < 5; ++$i) {
            $Event = new Event();
            $Events[] = [$Event];
        }

        return $Events;
    }
}
