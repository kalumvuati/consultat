<?php

namespace App\DataFixtures;

use App\Entity\Client;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ClientFixtures extends Fixture
{
    private UserPasswordEncoderInterface $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $limit = 10;

        //Create Client User
        for ($i = 0; $i < $limit; ++$i) {
            $user = new User();
            $client = new Client();
            //User
            $user->setPassword($this->encoder->encodePassword($user, 'client12'))
                ->setEmail("client$i@unixel.fr")
                ->setRoles(['ROLE_CLIENT'])
                ->setIsVerified($faker->boolean())
                ->setClient($client);
            //Client
            $client->setUser($user)
                ->setName($faker->name())
                ->setLastname($faker->lastName())
                ->setBirthday($faker->dateTime())
                ->setGoogleSub($faker->numberBetween(125478552, 225478552));
            $manager->persist($user);
            $manager->persist($client);
        }
        $manager->flush();
    }
}
