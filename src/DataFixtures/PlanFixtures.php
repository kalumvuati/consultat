<?php

namespace App\DataFixtures;

use App\Entity\Interval;
use App\Entity\Plan;
use App\Service\StripeHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PlanFixtures extends Fixture
{
    private UserPasswordEncoderInterface $encoder;
    private StripeHelper $helper;
    private string $stripeCurrency;

    public function __construct(UserPasswordEncoderInterface $encoder, StripeHelper $helper, string $stripeCurrency)
    {
        $this->encoder = $encoder;
        $this->helper = $helper;
        $this->stripeCurrency = $stripeCurrency;
    }

    public function load(ObjectManager $manager)
    {
        $plans = ['Individuel', 'Start Up', 'Group', 'Establishment', 'Default'];
        $intervals = ['day', 'weeks', 'month', 'year'];

        $faker = Factory::create('fr_FR');
        $plans_stripes = $this->helper->getPlans();
        if (count($plans_stripes)) {
            /** @var \Stripe\Plan $plan_stripe */
            foreach ($plans_stripes as $key => $plan_stripe) {
                //Create interval for plan
                $interval = (new Interval())->setTitle($intervals[$key]);

                //Create Plan
                $plan = new Plan();
                $plan->setTitle($plan_stripe->nickname)
                    ->setSubscriptionId($plan_stripe->id)
                    ->setPrice($plan_stripe->amount / 100)
                    ->setIntervale($interval)
                    ->setCurrency($plan_stripe->currency)
                    ->setDescription($faker->text())
                    ->setIsVisible(true);

                $manager->persist($interval);
                $manager->persist($plan);
            }
        } else {
            //Create Interval
            foreach ($plans as $key => $plan_v) {
                $plan = new Plan();
                //Create interval for plan
                $interval = (new Interval())->setTitle($intervals[$key]);
                //Create Currency
                $plan->setTitle($plan_v)
                    ->setPrice($faker->randomFloat(2, 12.35, 356.45))
                    ->setIntervale($interval)
                    ->setCurrency($this->stripeCurrency)
                    ->setDescription($faker->text());
                $manager->persist($interval);
                $manager->persist($plan);
            }
        }
        $manager->flush();
    }
}
