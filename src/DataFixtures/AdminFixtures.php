<?php

namespace App\DataFixtures;

use App\Entity\Admin;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminFixtures extends Fixture
{
    private UserPasswordEncoderInterface $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Factory::create('fr_FR');
        $limit = $faker->numberBetween(5, 10);

        //Create Admin User
        for ($i = 0; $i < $limit; ++$i) {
            $user = new User();
            $admin = new Admin();
            $user->setPassword($this->encoder->encodePassword($user, 'admin'))
                ->setEmail("admin$i@unixel.fr")
                ->setRoles(['ROLE_ADMIN'])
                ->setIsVerified(true)
                ->setAdministrator($admin);
            $admin->setUser($user);
            $manager->persist($user);
            $manager->persist($admin);
        }
        $manager->flush();
    }
}
