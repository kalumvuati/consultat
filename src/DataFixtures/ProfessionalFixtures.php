<?php

namespace App\DataFixtures;

use App\Entity\Event;
use App\Entity\Professional;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ProfessionalFixtures extends Fixture
{
    private UserPasswordEncoderInterface $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $limit = 10;

        //Create Client User
        for ($i = 0; $i < $limit; ++$i) {
            $user = new User();
            $professional = new Professional();
            //User
            $user->setPassword($this->encoder->encodePassword($user, 'professional'))
                ->setEmail("professional$i@unixel.fr")
                ->setRoles(['ROLE_PROFESSIONAL'])
                ->setIsVerified($faker->boolean())
                ->setClient($professional);
            //Client
            $professional->setUser($user)
                ->setName($faker->name())
                ->setLastname($faker->lastName())
                ->setBirthday($faker->dateTime())
                ->setGoogleSub($faker->numberBetween(125478552, 225478552))
                ->setGoogleSub((string) $faker->numberBetween(125478552, 225478552))
                ->setSiret('12345678912345');
            $manager->persist($user);
            $manager->persist($professional);

            //Event
            $event_number = 2;
            $start = new \DateTime();
            $end = new \DateTime('+1 day');
            for ($j = 0; $j < $event_number; ++$j) {
                $event = new Event();
                $event->setProfessional($professional)
                    ->setStartAt($start)
                    ->setEndAt($end)
                    ->setDescription($faker->text(150))
                    ->setTitle($faker->text(100));
                $isAborted = $faker->boolean();
                if ($isAborted) {
                    $event->setIsAborted($isAborted)
                        ->setReason($faker->text(200));
                } else {
                    $toValidate = $faker->boolean();
                    if ($toValidate) {
                        $event->setValidatedAt($faker->dateTime())
                            ->setIsFree(false);
                    }
                }
                $manager->persist($event);
            }
        }
        $manager->flush();
    }
}
