<?php

namespace App\MessageHandler;

use App\Message\MailerNotification;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class MailerNotificationHandler implements MessageHandlerInterface
{
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function __invoke(MailerNotification $notification)
    {
        dump('Je suis le message à être envoyé');
        $notification->registerProfessional($this->mailer);
    }
}
