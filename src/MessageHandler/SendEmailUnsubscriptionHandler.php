<?php

namespace App\MessageHandler;

use App\Entity\Client;
use App\Message\SendEmailUnsubscription;
use App\Service\SendEmailService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class SendEmailUnsubscriptionHandler implements MessageHandlerInterface
{
    private EntityManagerInterface $manager;
    private SendEmailService $service;

    public function __construct(EntityManagerInterface $manager, SendEmailService $service)
    {
        $this->manager = $manager;
        $this->service = $service;
    }

    public function __invoke(SendEmailUnsubscription $message)
    {
        if ($client = $this->manager->find(Client::class, $message->getClientId())) {
            $this->service->notifyUnsubscribedClient($client);
        }
    }
}
