<?php

namespace App\MessageHandler;

use App\Entity\Professional;
use App\Message\ProfessionalNotificationMessage;
use App\Service\ProfessionalNotify;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ProfessionalNotificationHandler implements MessageHandlerInterface
{
    private EntityManagerInterface $manager;
    private ProfessionalNotify $notify;

    public function __construct(EntityManagerInterface $manager, ProfessionalNotify $notify)
    {
        $this->manager = $manager;
        $this->notify = $notify;
    }

    public function __invoke(ProfessionalNotificationMessage $message)
    {
        $id = $message->getProfessionalId();
        $prof_repo = $this->manager->getRepository(Professional::class);
        $professional = $prof_repo->find($id);
        if ($professional) {
            $this->notify->notify($professional);
        }
    }
}
