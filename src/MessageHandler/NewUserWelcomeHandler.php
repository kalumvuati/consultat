<?php

namespace App\MessageHandler;

use App\Entity\Client;
use App\Message\NewUserWelcome;
use App\Service\SendEmailService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class NewUserWelcomeHandler implements MessageHandlerInterface
{
    private MailerInterface $mailer;
    private EntityManagerInterface $manager;

    public function __construct(EntityManagerInterface $manager, SendEmailService $mailer)
    {
        $this->mailer = $mailer;
        $this->manager = $manager;
    }

    public function __invoke(NewUserWelcome $welcome)
    {
        if ($client = $this->manager->find(Client::class, $welcome->getId())) {
            $this->mailer->sayWelcomeToNewUser($client);
        }
    }
}
