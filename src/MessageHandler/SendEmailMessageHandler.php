<?php

namespace App\MessageHandler;

use App\Entity\Client;
use App\Entity\Professional;
use App\Message\SendEmailMessage;
use App\Service\SendEmailService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class SendEmailMessageHandler implements MessageHandlerInterface
{
    private EntityManagerInterface $manager;
    private SendEmailService $service;

    public function __construct(EntityManagerInterface $manager, SendEmailService $service)
    {
        $this->manager = $manager;
        $this->service = $service;
    }

    public function __invoke(SendEmailMessage $message)
    {
        // do something with your message
        $client = $this->manager->find(Client::class, $message->getClientId());
        $professional = $this->manager->find(Professional::class, $message->getProfessionalId());
        if ($client && $professional) {
            $this->service->notifyProfessional($client, $professional, $message->getTemplate(), $message->isInverse());
        }
    }
}
