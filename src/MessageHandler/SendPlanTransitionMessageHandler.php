<?php

namespace App\MessageHandler;

use App\Entity\Professional;
use App\Message\SendPlanTransitionMessage;
use App\Service\SendEmailService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class SendPlanTransitionMessageHandler implements MessageHandlerInterface
{
    private EntityManagerInterface $manager;
    private SendEmailService $service;

    public function __construct(EntityManagerInterface $manager, SendEmailService $service)
    {
        $this->manager = $manager;
        $this->service = $service;
    }

    public function __invoke(SendPlanTransitionMessage $message)
    {
        foreach ($message->getProfessionalsId() as $id) {
            if ($professional = $this->manager->find(Professional::class, $id)) {
                $this->service->notifyProfessionalForPlanDeleted($professional);
            }
        }
    }
}
