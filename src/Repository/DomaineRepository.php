<?php

namespace App\Repository;

use App\Entity\Domaine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Domaine|null find($id, $lockMode = null, $lockVersion = null)
 * @method Domaine|null findOneBy(array $criteria, array $orderBy = null)
 * @method Domaine[]    findAll()
 * @method Domaine[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DomaineRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Domaine::class);
    }

    public function findByDomaine(array $start_end)
    {
        $start = strtolower($start_end[0] ?? '');
        $end = strtolower($start_end[1] ?? '');

        return $this->createQueryBuilder('d')
            ->andWhere('lower(d.domaine) LIKE :start')
            ->setParameter('start', "%$start%")
            ->orWhere('lower(d.domaine) LIKE :end')
            ->setParameter('end', "%$end%")
            ->orderBy('d.id', 'ASC')->getQuery()->getResult();
    }
}
