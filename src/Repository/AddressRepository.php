<?php

namespace App\Repository;

use App\Entity\Address;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Address|null find($id, $lockMode = null, $lockVersion = null)
 * @method Address|null findOneBy(array $criteria, array $orderBy = null)
 * @method Address[]    findAll()
 * @method Address[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AddressRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Address::class);
    }

    public function findByAddressInfo(string $addressInfo)
    {
        $lower = strtolower($addressInfo);
        $treated = explode(" ", $lower);
        $query = $this->createQueryBuilder('a');
        foreach ($treated as $traite) {
            $query = $query->orWhere('lower(a.address) LIKE :address')
                ->setParameter('address', "%$traite%")
                ->orWhere('lower(a.city) LIKE :city')
                ->setParameter('city', "%$traite%")
                ->orWhere('lower(a.zipcode) LIKE :zipcode')
                ->setParameter('zipcode', "%$traite%")
                ->orWhere('lower(a.transportIndicationProf) LIKE :indication')
                ->setParameter('indication', "%$traite%");
        }
        return $query->orderBy('a.id', 'ASC')->distinct(true)->getQuery()->getResult();
    }

    // /**
    //  * @return Address[] Returns an array of Address objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Address
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
