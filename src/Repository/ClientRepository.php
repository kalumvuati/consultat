<?php

namespace App\Repository;

use App\Entity\Client;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Client::class);
    }

    public function findOrCreateUserFromGoogleOauth(ResourceOwnerInterface $owner)
    {
        /** @var Client $client */
        $client = $this->createQueryBuilder('c')
            ->andWhere('c.googleSub = :googleSub')
            ->setParameter('googleSub', $owner->getId())
            ->getQuery()->getOneOrNullResult();
        if (!$client) {
            $owner_array = $owner->toArray();
            $user = new User();
            $user->setEmail($owner_array['email'])
                ->setIsVerified($owner_array['email_verified'])
                ->setRoles(['ROLE_CLIENT']);

            $client = new Client();
            $client->setName($owner_array['family_name'])
                ->setLastname($owner_array['given_name'])
                ->setGoogleSub($owner->getId());

            $this->_em->persist($user);
            $user->setClient($client);
            $this->_em->persist($client);
            $this->_em->flush();
        }

        return $client->getUser();
    }

    /*
    public function findOneBySomeField($value): ?Client
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
