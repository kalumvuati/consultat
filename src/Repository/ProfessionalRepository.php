<?php

namespace App\Repository;

use App\Entity\Professional;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Professional|null find($id, $lockMode = null, $lockVersion = null)
 * @method Professional|null findOneBy(array $criteria, array $orderBy = null)
 * @method Professional[]    findAll()
 * @method Professional[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProfessionalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Professional::class);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findOneBySiret(string $siret)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.siret = :siret')
            ->setParameter('siret', $siret)
            ->getQuery()->getOneOrNullResult();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findOneByUser(int $user_id)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.user = :user_id')
            ->setParameter('user_id', $user_id)
            ->getQuery()->getOneOrNullResult();
    }

    /**
     * Does searches on database according options given.
     */
    public function findByOptions(array $options): array
    {
        $query = $this->createQueryBuilder('p');
        return $query->getQuery()->getResult();
    }
}
