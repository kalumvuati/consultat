<?php

namespace App\Service;

use App\Entity\Client;
use App\Entity\PaymentMethod;
use App\Entity\Plan;
use App\Entity\Professional;
use Doctrine\ORM\EntityManagerInterface as Em;
use Stripe\Customer;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent;
use Stripe\Stripe;
use Stripe\StripeClient;

class StripeHelper
{
    /**
     * secret api key.
     */
    private string $api_key;

    private StripeClient $stripeClient;
    private Em $manager;
    private string $currency;
    private string $message;
    private bool $error;

    public function __construct(string $stripeApiKey, Em $manager, string $stripeCurrency = 'eur')
    {
        $this->api_key = $stripeApiKey;
        $this->stripeClient = new StripeClient($this->api_key);
        //Used for buying module/cours
        Stripe::setApiKey($this->api_key);
        $this->manager = $manager;
        $this->currency = $stripeCurrency;
    }

    /**
     * To create Payment Intent, used when client want to buy something directly.
     *
     * @return PaymentIntent
     *
     * @throws ApiErrorException
     */
    public function createPI(array $params)
    {
        if (isset($params['amount']) && !empty($params['amount'])) {
            $params['amount'] *= 100; // cents to Euro
        }

        return PaymentIntent::create($params);
    }

    /**
     * return Stripe object.
     */
    public function getStripeClient(): StripeClient
    {
        return $this->stripeClient;
    }

    /**
     * Allow Professional to subscribe a plan.
     *
     * @param Plan $plan plan that Professional will subscribe
     *
     * @throws ApiErrorException
     */
    public function subscription(Professional $professional, Plan $plan): void
    {
        $clientStripe = $this->getStripeClient();
        //Plan to subscribe
        $plan_v = $this->stripeClient->plans->retrieve($plan->getSubscriptionId());
        $item = [['price' => $plan_v->id]];
        $customer = $this->generateCustomer($professional);
        $customer_obj = ['customer' => $customer->id, 'items' => $item];
        $subscriber = $clientStripe->subscriptions->create($customer_obj);

        //Update plan infos
        $professional->setSubscriptionStripeId($subscriber->id);
        $professional->setPlan($plan);
        $this->manager->flush();
    }

    public function unSubscription(string $subscription_id)
    {
        $message = 'Customer was deleted successfully';
        try {
            $subscription = $this->stripeClient->subscriptions->retrieve($subscription_id);
            $this->stripeClient->subscriptions->cancel($subscription->id);
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
        }

        return $message;
    }

    /**
     * method for create a plan according price, interval, ...
     * This function create a plan, and when this one is created and used
     * No change will be accepted.
     *
     * @return bool
     *
     * @throws ApiErrorException
     */
    public function createPlan(Plan $plan_p)
    {
        $exists = false;
        if ($plan_p) {
            //Checking if plan exits
            $plans_data = $this->stripeClient->plans->all()->data;
            foreach ($plans_data as $plan_) {
                if (strtolower($plan_->nickname) === strtolower($plan_p->getTitle())) {
                    $exists = true;
                    //Upload Info
                    $plan_p->setSubscriptionId($plan_->id);
                    break;
                }
            }

            //To create just if isn't exist
            if (!$exists) {
                //Create Product
                $product = $this->stripeClient->products->create(
                    [
                        'name' => $plan_p->getTitle(),
                        'active' => true,
                        'description' => $plan_p->getDescription(),
                    ]
                );

                //Create Plan for a product
                $plan = $this->stripeClient->plans->create(
                    [
                        'amount' => $plan_p->getPrice() * 100,
                        'currency' => $this->currency,
                        'interval' => $plan_p->getIntervale()->getTitle(),
                        'product' => $product->id,
                        'nickname' => $plan_p->getTitle(),
                    ]
                );

                //update plan info
                $plan_p->setSubscriptionId($plan->id);
            }

            $this->manager->persist($plan_p);
            //flus must br called where this function will be usedstrip
            //$this->manager->flush();
        }

        return $exists;
    }

    /**
     * This function seek for every client's subscriptions and unsubscribed her/him from
     * an delete her/him as customer.
     *
     * @return string
     */
    public function deleteCustomer(string $customer_id, string $subscription_id = '')
    {
        $isDelete = 'Customer was unsubscribed successfully';
        try {
            $customer = $this->stripeClient->customers->retrieve($customer_id);
            //Unsubscription
            $isDelete = $this->unSubscription($subscription_id);
            $this->stripeClient->customers->delete($customer->id);
        } catch (\Exception $exception) {
            $isDelete = $exception->getMessage();
        }

        return $isDelete;
    }

    public function getPlans()
    {
        return $this->stripeClient->plans->all();
    }

    /**
     * @throws ApiErrorException
     */
    private function createPrice(Plan $plan)
    {
        $stripePlan = $this->stripeClient->plans->retrieve($plan->getSubscriptionId());
        $produit_link_to_plan = $this->stripeClient->products->retrieve($stripePlan->product);

        return $this->stripeClient->prices->create(
            [
                'product' => $produit_link_to_plan->id,
                'unit_amount' => $plan->getPrice() * 100,
                'currency' => $this->currency,
            ]
        );
    }

    /**
     * @return bool
     */
    public function detelePlan(string $plan_id)
    {
        $isDelete = true;
        try {
            $plan = $this->stripeClient->plans->retrieve($plan_id);
            $product = $this->stripeClient->products->retrieve($plan->product);
            $this->stripeClient->plans->delete($plan->id);
            $this->stripeClient->products->delete($product->id);
        } catch (\Exception $exception) {
            $isDelete = false;
        }

        return $isDelete;
    }

//    public function getPlanInvoice(Professional $Professional)
//    {
//        $invoices = [];
//        if ($Professional && $Professional->getCustomerId()) {
//            $invoices_temp = $this->stripeClient->invoices->all();
//            /** @var InvoiceAlias $invoice */
//            foreach ($invoices_temp as $invoice) {
//                if ($invoice->customer === $Professional->getCustomerId()) {
//                    $invoices[] = $invoice;
//                }
//            }
//        }
//        return $invoices;
//    }
//
//    public function sendInvoice(Professional $Professional, Module $module)
//    {
//        //Create price
//        //$price = $this->stripeClient->prices->create();
//        $invoiceToSend = $this->stripeClient->invoiceItems->create(
//            [
//                'price' => $module->getPriceId(),
//                'customer' => $Professional->getCustomerId(),
//            ]
//        );
//        $invoice = $this->stripeClient->invoices->create(
//            [
//                'customer' => $Professional->getCustomerId(),
//                'collection_method' => 'send_invoice',
//                'days_until_due' => 30,
//            ]
//        );
//        $invoice->sendInvoice();
//    }

    /**
     * @return Customer | null
     *
     * @throws ApiErrorException
     */
    public function createCustomer(Professional $professional, PaymentMethod $method): ?Customer
    {
        $customer = null;
        if ($professional && !$professional->getCustomerId()) {
            //Attach Payment Method
            try {
                $payMethod = $this->stripeClient
                    ->paymentMethods->create([
                        'type' => 'card',
                        'card' => [
                            'number' => $method->getCardNumber(),
                            'exp_month' => $method->getCardMonth(),
                            'exp_year' => $method->getCardYear(),
                            'cvc' => $method->getCardCvc(),
                        ],
                    ]);
                //Create customer
                $customer = $this->stripeClient->customers->create([
                    'email' => $professional->getUser()->getEmail(),
                    'name' => $method->getCardNames(),
                    'payment_method' => $payMethod->id,
                    'invoice_settings' => [
                        'default_payment_method' => $payMethod->id,
                    ],
                ]);

                //Attach Payment Method to a client
                $attach = $this->stripeClient->paymentMethods->attach($payMethod->id, ['customer' => $customer->id]);

                //update User/Client infos
                $professional->setCustomerId($customer->id);
                $method->setStripeId($payMethod->id);
                $method->setProfessional($professional);
                $this->manager->persist($method);
                $this->manager->flush();
            } catch (\Exception $exception) {
                $this->error = true;
                $this->message = $exception->getMessage();
            }
        } else {
            $customer = $this->stripeClient->customers->retrieve($professional->getCustomerId());
        }
        return $customer;
    }

    /**
     * This function is called with Professional having at least one PaymentMethod.
     *
     * @throws ApiErrorException
     */
    public function generateCustomer(Professional $professional): Customer
    {
        $customer = null;
        if ($professional && !$professional->getCustomerId()) {
            $paymentMethodActived = null;
            //Search for an active payment method
            foreach ($professional->getPaymentMethods() as $method) {
                if ($method->getIsActived()) {
                    $paymentMethodActived = $method;
                    break;
                }
            }
            //Create customer
            try {
                $payMethod = $this->stripeClient
                    ->paymentMethods->create([
                        'type' => 'card',
                        'card' => [
                            'number' => $paymentMethodActived->getCardNumber(),
                            'exp_month' => $paymentMethodActived->getCardMonth(),
                            'exp_year' => $paymentMethodActived->getCardYear(),
                            'cvc' => $paymentMethodActived->getCardCvc(),
                        ],
                    ]);

                $customer = $this->stripeClient->customers->create([
                    'email' => $professional->getUser()->getEmail(),
                    'name' => $paymentMethodActived->getCardNames(),
                    'payment_method' => $payMethod->id,
                    'invoice_settings' => [
                        'default_payment_method' => $payMethod->id,
                    ],
                ]);
                //Attach Payment Method to a client
                $attach = $this->stripeClient->paymentMethods->attach($payMethod->id, ['customer' => $customer->id]);
            } catch (\Exception $exception) {
                $this->error = true;
                $this->message = $exception->getMessage();
            }

            //update User/Client infos
            $professional->setCustomerId($customer->id);
            $this->manager->flush();
        } else {
            $customer = $this->stripeClient->customers->retrieve($professional->getCustomerId());
        }

        return $customer;
    }

    public function createPaymentMethod(Professional $professional, PaymentMethod $method, string $customerId)
    {
        //Attach Payment Method
        try {
            $payMethod = $this->stripeClient
                ->paymentMethods->create([
                    'type' => 'card',
                    'card' => [
                        'number' => $method->getCardNumber(),
                        'exp_month' => $method->getCardMonth(),
                        'exp_year' => $method->getCardYear(),
                        'cvc' => $method->getCardCvc(),
                    ], ]);
        } catch (\Exception $exception) {
            $this->message = $this->getMessage();
        }

        //Create customer
        try {
            $customer = $this->stripeClient->customers->create([
                'email' => $professional->getUser()->getEmail(),
                'name' => $method->getCardNames(),
                'payment_method' => $payMethod->id,
                'invoice_settings' => [
                    'default_payment_method' => $payMethod->id,
                ], ]);
        } catch (\Exception $exception) {
            $this->message = $this->getMessage();
        }

        //Attach Payment Method to a client
        $attach = $this->stripeClient->paymentMethods->attach($payMethod->id, ['customer' => $customer->id]);

        return $payMethod;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function isError(): bool
    {
        return $this->error;
    }
}
