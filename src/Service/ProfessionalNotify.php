<?php

namespace App\Service;

use App\Entity\Professional;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class ProfessionalNotify
{
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function notify(Professional $professional)
    {
        $email = new TemplatedEmail();
        $from = new Address('noreplay@consulting.fr', 'Register Service');
        $to = new Address($professional->getUser()->getEmail(), $professional->getName());
        $template = 'professional/email/register_notify.html.twig';
        $email->from($from)->to($to)->htmlTemplate($template);

        $this->mailer->send($email);
    }
}
