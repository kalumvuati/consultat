<?php

namespace App\Service;

use App\Entity\Client;
use App\Entity\Professional;
use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;

class SendEmailService
{
    private MailerInterface $mailer;
    private TranslatorInterface $trans;
    private ParameterBagInterface $bag;

    public function __construct(MailerInterface $mailer, TranslatorInterface $trans, ParameterBagInterface $bag)
    {
        $this->mailer = $mailer;
        $this->trans = $trans;
        $this->bag = $bag;
    }

    /**
     * @param bool $inversed true when the message is to be sent to Client else Professional
     *
     * @throws TransportExceptionInterface
     */
    public function notifyProfessional(Client $client, Professional $prof, string $template, bool $inversed = false)
    {
        try {
            $template_v = 'professional/email/event/'.$template.'.html.twig';
            $from = new Address($client->getUser()->getEmail(), $client->getName());
            $to = new Address($prof->getUser()->getEmail(), $prof->getName());
            $subject = $this->trans->trans('News about Appointment');
            if ($inversed) {
                $template_v = 'client/email/event/'.$template.'.html.twig';
                $to = new Address($client->getUser()->getEmail(), $client->getName());
                $from = new Address($prof->getUser()->getEmail(), $prof->getName());
            }
            $email = (new TemplatedEmail())
                ->from($from)
                ->to($to)
                ->subject($subject)
                ->context(['professional' => $prof, 'client' => $client])
                ->htmlTemplate($template_v);
            $this->mailer->send($email);
        } catch (\Exception $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }
    }

    public function notifyProfessionalForPlanDeleted(Professional $prof)
    {
        try {
            $template_v = 'professional/email/plan/plan_deleted.html.twig';
            $from = new Address($this->bag->get('noreplay.mailer'), 'TEMPORARY PLAN');
            $to = new Address($prof->getUser()->getEmail(), $prof->getName());
            $subject = $this->trans->trans('Change to the temporary plan');
            $email = (new TemplatedEmail())
                ->from($from)
                ->to($to)
                ->subject($subject)
                ->context(['professional' => $prof])
                ->htmlTemplate($template_v);
            $this->mailer->send($email);
        } catch (\Exception $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }
    }

    public function sayWelcomeToNewUser(Client $client)
    {
        try {
            $template_v = 'client/email/welcome.html.twig';
            $from = new Address($this->bag->get('noreplay.mailer'), 'Welcome');
            $to = new Address($client->getUser()->getEmail(), $client->getName());
            $subject = $this->trans->trans('We welcome you for your subscription');
            $email = (new TemplatedEmail())
                ->from($from)
                ->to($to)
                ->subject($subject)
                ->context(['client' => $client])
                ->htmlTemplate($template_v);
            $this->mailer->send($email);
        } catch (\Exception $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }
    }

    public function notifyUnsubscribedClient(Client $client)
    {
        try {
            $template_v = 'client/email/unsubscribe.html.twig';
            $from = new Address($this->bag->get('noreplay.mailer'), 'Notify service');
            $to = new Address($client->getUser()->getEmail(), $client->getName());
            $subject = $this->trans->trans('Notification');
            $email = (new TemplatedEmail())
                ->from($from)
                ->to($to)
                ->subject($subject)
                ->context(['client' => $client])
                ->htmlTemplate($template_v);
            $this->mailer->send($email);
        } catch (\Exception $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }
    }
}
