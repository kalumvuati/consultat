<?php

namespace App\Service;

use GuzzleHttp\Client;
use Sirene\Client\Api\EtablissementApi;
use Sirene\Client\Configuration;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

class SiretHelper
{
    private string $siren_api_token;
    private TranslatorInterface $translator;

    public function __construct(string $siren_api_token, TranslatorInterface $translator)
    {
        $this->siren_api_token = $siren_api_token;
        $this->translator = $translator;
    }

    /**
     * @param $siret Enterprise SIRET CODE (14 characters)
     *
     * @return array
     */
    public function checkSiret($siret)
    {
        $client = new Client();
        $config = new Configuration();
        $config->setAccessToken($this->siren_api_token);
        $apiSirent = new EtablissementApi($client, $config);
        try {
            $result = $apiSirent->findBySiret($siret);
            $stab = $result->getEtablissement();
            $unitLegale = $stab->getUniteLegale();
            if (!$enterprise = $unitLegale->getDenominationUniteLegale()) {
                $enterprise = $unitLegale->getNomUniteLegale();
            }
            $message = $this->translator->trans('Welcome '.$enterprise);
            $response_data = ['siret' => $stab->getSiret(), 'denomination' => $enterprise, 'message' => $message];
        } catch (\Exception $exception) {
            $response_data = ['message' => $exception->getMessage(), 'status' => Response::HTTP_UNAUTHORIZED];
        }

        return $response_data;
    }

    public function getToken()
    {
        return $this->siren_api_token;
    }
}
