<?php

namespace App\Security;

use App\Entity\Client;
use Doctrine\ORM\EntityManagerInterface as Em;
use Google\Auth\AccessToken;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Security\Authenticator\SocialAuthenticator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class GoogleAuthenticator extends SocialAuthenticator
{
    private RouterInterface $router;
    private ClientRegistry $registry;
    private Em $em;

    public function __construct(RouterInterface $router, ClientRegistry $registry, Em $em)
    {
        $this->router = $router;
        $this->registry = $registry;
        $this->em = $em;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new RedirectResponse($this->router->generate('app_login'));
    }

    public function supports(Request $request)
    {
        return 'login_check' === $request->attributes->get('_route') && 'google' === $request->get('service');
    }

    public function getCredentials(Request $request)
    {
        return $this->fetchAccessToken($this->getClient('google_main'));
    }

    /**
     * @param AccessToken $credentials
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $google_client = $this->getClient('google_main')->fetchUserFromToken($credentials);
        return $this->em->getRepository(Client::class)->findOrCreateUserFromGoogleOauth($google_client);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new RedirectResponse($this->router->generate('app_login'));
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey)
    {
        return new RedirectResponse($this->router->generate('client_profile'));
    }

    private function getClient(string $client)
    {
        return $this->registry->getClient($client);
    }
}
