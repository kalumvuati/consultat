<?php

namespace App\Message;

class ProfessionalNotificationMessage
{
    private int $professional_id;

    public function __construct(int $professional_id)
    {
        $this->professional_id = $professional_id;
    }

    public function getProfessionalId(): int
    {
        return $this->professional_id;
    }
}
