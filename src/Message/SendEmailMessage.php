<?php

namespace App\Message;

final class SendEmailMessage
{
    private int $client_id;
    private int $professional_id;
    private string $template;
    private bool $inversed;

    public function __construct(int $client_id, int $professional_id, string $template, bool $inversed = false)
    {
        $this->client_id = $client_id;
        $this->professional_id = $professional_id;
        $this->template = $template;
        $this->inversed = $inversed;
    }

    /**
     * @return int
     */
    public function getClientId(): int
    {
        return $this->client_id;
    }

    /**
     * @return int
     */
    public function getProfessionalId(): int
    {
        return $this->professional_id;
    }

    /**
     * @return string
     */
    public function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * @return bool
     */
    public function isInverse(): bool
    {
        return $this->inversed;
    }
}
