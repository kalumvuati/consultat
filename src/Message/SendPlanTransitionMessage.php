<?php

namespace App\Message;

final class SendPlanTransitionMessage
{
    private array $professionals_id;

    public function __construct(array $professionals_id)
    {
        $this->professionals_id = $professionals_id;
    }

    /**
     * @return array
     */
    public function getProfessionalsId(): array
    {
        return $this->professionals_id;
    }
}
