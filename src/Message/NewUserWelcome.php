<?php

namespace App\Message;

class NewUserWelcome
{
    private int $use_id;

    public function __construct(int $use_id)
    {
        $this->use_id = $use_id;
    }

    public function getId()
    {
        return $this->use_id;
    }
}
