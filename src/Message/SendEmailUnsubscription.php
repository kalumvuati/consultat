<?php

namespace App\Message;

final class SendEmailUnsubscription
{
    private int $client_id;

    public function __construct(int $client_id)
    {
        $this->client_id = $client_id;
    }

    /**
     * @return int
     */
    public function getClientId(): int
    {
        return $this->client_id;
    }
}
