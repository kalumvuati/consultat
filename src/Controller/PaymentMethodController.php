<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Event;
use App\Entity\PaymentMethod;
use App\Repository\PaymentMethodRepository;
use Doctrine\ORM\EntityManagerInterface as Manager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface as Trans;

/**
 * @Route("/paymentmethod", name="paymentmethod")
 */
class PaymentMethodController extends AbstractController
{
    private PaymentMethodRepository $repository;
    private Manager $manager;
    private Trans $translator;

    public function __construct(PaymentMethodRepository $repository, Manager $manager, Trans $translator)
    {
        $this->repository = $repository;
        $this->manager = $manager;
        $this->translator = $translator;
    }

    /**
     * @IsGranted("ROLE_CLIENT")
     * @Route("/add", name="_add_client", methods={"POST"})
     * @Route("/edit/{slug}", name="_edit_client", methods={"POST"})
     */
    public function addPaymentMethod(PaymentMethod $paymentMethod = null, Request $request)
    {
        //$message = $this->translator->trans("Something wrong happened during this change");
        $type = 'success';

        $isNew = false;
        if (!$paymentMethod) {
            $isNew = true;
            $paymentMethod = new PaymentMethod();
        }
        /** @var Client $client */
        $client = $this->getUser()->getClient();
        //Hydratation
        $paymentMethod->setCardNames($request->get('cardNames'))
            ->setCardNumber($request->get('cardNumber'))
            ->setCardMonth($request->get('cardMonth'))
            ->setCardYear($request->get('cardYear'))
            ->setCardCvc($request->get('cardCvc'));
        if ($isNew) {
            $paymentMethod->setClient($client);
            $client->setPaymentMethod($paymentMethod);
            $this->manager->persist($paymentMethod);
            $message = $this->translator->trans('Payment method was added successfully');
            $paymentMethod->setIsActived(true);
        } else {
            //Edit
            $message = $this->translator->trans('Payment method was edited successfully');
        }

        $this->manager->flush();
        $this->addFlash($type, $message);

        return $this->redirectToRoute('client_profile');
    }

    /**
     * @IsGranted("ROLE_CLIENT")
     * @Route("/{slug}/delete", name="_delete", methods={"DELETE"})
     *
     * @return RedirectResponse
     */
    public function delete(PaymentMethod $paymentMethod, Request $request)
    {
        //If appointment are presents, impossible to delete card
        $message = $this->translator->trans('Delete PaymentMethod failed');
        $key = 'error';
        if ($this->isCsrfTokenValid('delete'.$paymentMethod->getSlug(), $request->get('_token'))) {
            $events = $this->getUser()->getClient()->getEvents();
            $isAnnulable = true;
            /** @var Event $event */
            foreach ($events as $event) {
                if ($isAnnulable = !$event->isAnnulable()) {
                    $isAnnulable = false;
                    break;
                }
            }
            if ($isAnnulable) {
                $paymentMethod->setClient(null);
                $this->manager->remove($paymentMethod);
                $this->manager->flush();
                $key = 'success';
                $message = $this->translator->trans('PaymentMethod deleted successfully');
            } else {
                $key = 'error';
                $message = 'You have appointment to pay. So you cannot delete your credit card.';
                $message .= ' You can edit it, if it\'s what you want.';
                $message = $this->translator->trans($message);
            }
        }

        $this->addFlash($key, $message);

        return $this->redirectToRoute('client_profile');
    }
}
