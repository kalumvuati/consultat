<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Message\SendEmailUnsubscription;
use App\Repository\ProfessionalRepository as ProfRepo;
use App\Service\StripeHelper;
use Doctrine\ORM\EntityManagerInterface as Em;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface as Encoder;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface as Translator;

class SecurityController extends AbstractController
{
    private Em $manager;
    private Translator $translator;
    private Encoder $encoder;
    private ProfRepo $repository;

    public function __construct(Em $manager, Translator $translator, Encoder $encoder, ProfRepo $repository)
    {
        $this->manager = $manager;
        $this->translator = $translator;
        $this->encoder = $encoder;
        $this->repository = $repository;
    }

    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils, ClientRegistry $registry): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('client_profile');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        $redirect_google_url = $registry->getClient('google_main')->redirect(['email']);
        $return_params = ['last_username' => $lastUsername, 'error' => $error, 'google_url' => $redirect_google_url];

        return $this->render('security/login.html.twig', $return_params);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        $message = 'This method can be blank - it will be intercepted by the logout key on your firewall.';
        throw new \LogicException($message);
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/edit", name="user_edit", methods={"GET", "POST"})
     */
    public function editUser(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $type = 'error';
            $message = $this->translator->trans('Something wrong happened during this modification');
            $user_modified = $request->get('user');
            $pass = $user_modified['password'] ?? '';
            $confirmPass = $user_modified['confirmPassword'] ?? '';
            if ($user_modified && $pass === $confirmPass) {
                $user->setPassword($this->encoder->encodePassword($user, $pass));
                $this->manager->flush();
                $type = 'success';
                $message = $this->translator->trans('User was modified successfully');
                $this->addFlash($type, $message);

                return $this->redirectToRoute('client_profile');
            } else {
                $type = 'error';
                $message = $this->translator->trans('Confirmation password not the same to password');
                $this->addFlash($type, $message);
            }
        }

        return $this->render('user/edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/remove", name="user_remove_unsubscribe", methods={"DELETE"})
     */
    public function delete(Request $request, StripeHelper $helper, MessageBusInterface $bus)
    {
        /** @var User $user */
        $user = $this->getUser();
        $client = $user->getClient();
        $message = $this->translator->trans('Something happened during this operation. Please, try later');
        if ($this->isCsrfTokenValid('delete'.$client->getId(), $request->get('_token'))) {
            //Verify if this client has event under 24 hours
            $events = $client->getEvents();
            $hasToPay = false;
            foreach ($events as $event) {
                if (($hasToPay = !$event->isAnnulable())) {
                    $hasToPay = true;
                    break;
                }
            }

            if (!$hasToPay) {
                if ($professional = $this->repository->findOneByUser($user->getId())) {
                    $helper->deleteCustomer($professional->getCustomerId());
                }
                //Remove payment Method
                if ($client->getPaymentMethod()) {
                    //TODO - Stripe
                    $paymentmethod = $client->getPaymentMethod();
                }
                //Remove all old Events
                foreach ($events as $event) {
                    $client->removeEvent($event);
                    $this->manager->remove($event);
                }

                $message = new SendEmailUnsubscription($client->getId());
                $bus->dispatch($message);

                $this->manager->remove($user);
                $this->manager->flush();

                return $this->redirectToRoute('home');
            } else {
                $message = 'It seams like you have appointment hold. Please contact the Professional.';
                $message = $this->translator->trans($message);
            }
        }

        $this->addFlash('error', $message);

        return $this->redirectToRoute('client_profile');
    }
}
