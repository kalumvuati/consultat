<?php

namespace App\Controller\Professional;

use App\Entity\Experience;
use App\Form\ExperienceType;
use App\Repository\ExperienceRepository as ExperienceRepo;
use App\Repository\ProfessionalRepository as ProfRepo;
use Doctrine\ORM\EntityManagerInterface as Em;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/professional/Experience", name="professional_Experience")
 * Class ExperienceController
 */
class ExperienceController extends AbstractController
{
    private TranslatorInterface $translator;
    private ExperienceRepo $repository;
    private ProfRepo $professionalRepository;
    private Em $manager;

    public function __construct(TranslatorInterface $translator, ExperienceRepo $repo, ProfRepo $profRepo, Em $em)
    {
        $this->translator = $translator;
        $this->repository = $repo;
        $this->professionalRepository = $profRepo;
        $this->manager = $em;
    }

    /**
     * @Route("/new", name="_new", methods={"GET", "POST"})
     * @Route("/{slug}/edit", name="_edit", methods={"GET", "POST"})
     */
    public function new(Experience $Experience = null, Request $request)
    {
        $button = $this->translator->trans('Edit');
        $message = $this->translator->trans('Experience was edited successfully');
        if (!$Experience) {
            $Experience = new Experience();
            $button = $this->translator->trans('Save');
            $message = $this->translator->trans('Experience was created successfully');
        }

        $form = $this->createForm(ExperienceType::class, $Experience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //check date interval
            $inverval = $Experience->getEndAt()->diff($Experience->getStartAt());

            if ('Save' === $button) {
                $Experience->addProfessional($this->getUser()->getClient());
                $this->manager->persist($Experience);
            }

            $this->manager->flush();
            $this->addFlash('success', $message);

            return $this->redirectToRoute('professional_profile');
        }
        $view = 'professional/experience/register_notify.html.twig';

        return $this->render($view, ['form' => $form->createView(), 'button' => $button]);
    }

    /**
     * @Route("/{slug}/delete", name="_delete", methods={"DELETE"})
     *
     * @return RedirectResponse
     */
    public function delete(Experience $Experience, Request $request)
    {
        $message = $this->translator->trans('Delete Experience failed');
        $key = 'error';
        if ($this->isCsrfTokenValid('delete'.$Experience->getSlug(), $request->get('_token'))) {
            $this->manager->remove($Experience);
            $this->manager->flush();
            $key = 'success';
            $message = $this->translator->trans('Experience deleted successfully');
        }

        $this->addFlash($key, $message);

        return $this->redirectToRoute('professional_profile');
    }
}
