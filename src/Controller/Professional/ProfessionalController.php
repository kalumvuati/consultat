<?php

namespace App\Controller\Professional;

use App\Entity\Address;
use App\Entity\Professional;
use App\Entity\User;
use App\Form\AddressType;
use App\Form\ProfessionalType;
use App\Message\SendEmailUnsubscription;
use App\Repository\ProfessionalRepository as ProfessionalRep;
use App\Service\StripeHelper;
use Doctrine\ORM\EntityManagerInterface as Manager;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface as Mailer;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface as Trans;

/**
 * @Route("/professional", name="professional")
 */
class ProfessionalController extends AbstractController
{
    private ProfessionalRep $repository;
    private Mailer $mailer;
    private Manager $manager;
    private Trans $translator;

    public function __construct(ProfessionalRep $repository, Mailer $mailer, Manager $manager, Trans $translator)
    {
        $this->repository = $repository;
        $this->mailer = $mailer;
        $this->manager = $manager;
        $this->translator = $translator;
    }

    /**
     * @Route("/profile", name="_profile", methods={"GET"})
     */
    public function profile()
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($this->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('admin_profile');
        }
        $professional = $this->repository->findOneByUser($user->getId());
        $calendars = [];
        $return_arr = ['professional' => $professional, 'calendars' => $calendars];
        return $this->render('professional/profile.html.twig', $return_arr);
    }

    /**
     * @Route("/profile/edit/{slug}", name="_profile_edit", methods={"GET", "POST"})
     */
    public function edit(Professional $professional, Request $request)
    {
        $form = $this->createForm(ProfessionalType::class, $professional);
        $formAddress = $this->createForm(AddressType::class, $professional->getAddress());
        $form->handleRequest($request);
        $formAddress->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $formAddress->isSubmitted() && $formAddress->isValid()) {
            //Hudratation - Professional
            /** @var Professional $professional */
            $professional = $request->get('professional');
            $address = $request->get('address');
            $address_prof = $professional->getAddress();
            if (!$address_prof) {
                $address_prof = new Address();
                $this->manager->persist($address_prof);
            }

            $address_prof->setAddress($address['address'])
                ->setZipcode($address['zipcode'])
                ->setCity($address['city'])
                ->setComplementProf($address['complementProf'])
                ->setTransportIndicationProf($address['transportIndicationProf']);
            $professional->setAddress($address_prof);

            //Hydratation - Address
            $this->manager->flush();
            $message = $this->translator->trans('Professional profile modified successfully');
            $this->addFlash('success', $message);

            return $this->redirectToRoute('professional_profile');
        }
        $addressF = $formAddress->createView();
        $return = ['professional' => $professional, 'formAddress' => $addressF, 'form' => $form->createView()];

        return $this->render('professional/edit.html.twig', $return);
    }

    /**
     * @Route("/delete", name="_delete", methods={"DELETE"})
     *
     * @throws NonUniqueResultException
     */
    public function delete(Request $request, StripeHelper $helper, MessageBusInterface $bus)
    {
        /** @var User $user */
        $user = $this->getUser();
        $professional = $this->repository->findOneByUser($user->getId());
        if ($this->isCsrfTokenValid('delete'.$professional->getSlug(), $request->get('_token'))) {
            //Remove this user from stripe plan also and delete it from Stripe customer
            if ($professional->getCustomerId()) {
                $helper->deleteCustomer($professional->getCustomerId());
            }

            //Remove payment method
            foreach ($professional->getPaymentMethods() as $paymentMethod) {
                $professional->removePaymentMethod($paymentMethod);
                $this->manager->remove($paymentMethod);
            }
            $this->manager->flush();

            //Message to Send to this professional
            $message = new SendEmailUnsubscription($user->getClient()->getId());
            $bus->dispatch($message);

            //$this->manager->remove($professional);
            $this->manager->remove($user->getClient());
            $this->manager->remove($user);
            $this->manager->flush();

            return $this->redirectToRoute('home');
        }
        $message = $this->translator->trans('Something happened during this operation. Please, try later');
        $this->addFlash('error', $message);

        return $this->redirectToRoute('app_login');
    }
}
