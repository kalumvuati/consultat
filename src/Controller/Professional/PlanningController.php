<?php

namespace App\Controller\Professional;

use App\Entity\Planning;
use App\Entity\Professional;
use App\Form\PlanningType;
use App\Repository\PlanningRepository;
use App\Repository\ProfessionalRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface as Translator;

/**
 * @Route("/professional/planning", name="professional_planning")
 */
class PlanningController extends AbstractController
{
    private Translator $translator;
    private ObjectManager $manager;
    /**
     * @var ProfessionalRepository|ObjectRepository
     */
    private $professionalRepository;
    /**
     * @var PlanningRepository|ObjectRepository
     */
    private $planningRepository;

    /**
     * PlanningController constructor.
     */
    public function __construct(EntityManagerInterface $manager, Translator $translator)
    {
        $this->manager = $manager;
        $this->planningRepository = $this->manager->getRepository(Planning::class);
        $this->professionalRepository = $this->manager->getRepository(Professional::class);
        $this->translator = $translator;
    }

    /**
     * @Route("/new", name="_new", methods={"GET", "POST"})
     * @Route("/{slug}/edit", name="_edit", methods={"GET", "POST"})
     */
    public function new(Planning $planning = null, Request $request)
    {
        $button = $this->translator->trans('Edit');
        $message = $this->translator->trans('Planning was edited successfully');
        if (!$planning) {
            $planning = new Planning();
            $button = $this->translator->trans('Save');
            $message = $this->translator->trans('Planning was created successfully');
        }

        $form = $this->createForm(PlanningType::class, $planning);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ('Save' === $button) {
                $prof = $this->getUser()->getClient();
                $planning->setProfessional($prof);
                $this->manager->persist($planning);
            }

            $this->manager->flush();
            $this->addFlash('success', $message);

            return $this->redirectToRoute('professional_profile');
        }
        $view = 'professional/planning/new.html.twig';

        return $this->render($view, ['form' => $form->createView(), 'button' => $button]);
    }

    /**
     * @Route("/{slug}/delete", name="_delete", methods={"DELETE"})
     * @return RedirectResponse
     */
    public function delete(Planning $planning, Request $request)
    {
        $message = $this->translator->trans('Delete Planning failed');
        $key = 'error';
        if ($this->isCsrfTokenValid('delete'.$planning->getSlug(), $request->get('_token'))) {
            //TODO - check if exist event where are clients having appointments
            //TODO - To propose them an other day or time or cancel this appointment
            $this->manager->remove($planning);
            $this->manager->flush();
            $key = 'success';
            $message = $this->translator->trans('Planning deleted successfully');
        }

        $this->addFlash($key, $message);

        return $this->redirectToRoute('professional_profile');
    }
}
