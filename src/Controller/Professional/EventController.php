<?php

namespace App\Controller\Professional;

use App\Entity\Client;
use App\Entity\Event;
use App\Entity\Professional;
use App\Entity\User;
use App\Form\EventType;
use App\Message\SendEmailMessage;
use App\Repository\EventRepository;
use App\Repository\ProfessionalRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface as Em;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ObjectRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface as Translator;

/**
 * @Route("/professional/event", name="professional_event")
 */
class EventController extends AbstractController
{
    private Translator $translator;
    private ObjectManager $manager;
    /**
     * @var EventRepository|ObjectRepository
     */
    private $eventRepository;
    /**
     * @var ProfessionalRepository|ObjectRepository
     */
    private $clientRepository;
    private ProfessionalRepository $profRepo;
    private MessageBusInterface $bus;

    /**
     * EventController constructor.
     */
    public function __construct(Em $manager, Translator $translator, MessageBusInterface $bus)
    {
        $this->manager = $manager;
        $this->eventRepository = $this->manager->getRepository(Event::class);
        $this->clientRepository = $this->manager->getRepository(Client::class);
        $this->translator = $translator;
        $this->profRepo = $this->manager->getRepository(Professional::class);
        $this->bus = $bus;
    }

    /**
     * @Route("/index", name="_index", methods={"GET"})
     */
    public function index(): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $eventRepo = $this->getDoctrine()->getRepository(Event::class);
        $professional = $this->profRepo->findOneByUser($user->getId());
        $view = 'event/client/index.html.twig';
        if ($this->isGranted('ROLE_PROFESSIONAL')) {
            //Only Event not aborted or no canceled
            $events = $eventRepo->findBy(['professional' => $professional->getId()]);
            $view = 'professional/event/index.html.twig';
        } else {
            $events = $user->getClient()->getEvents();
        }
        $eventsFull = [];
        //Convert php event to Json
        foreach ($events as $event) {
            $eventsFull[] = self::eventToJson($event);
        }
        $return = ['events' => $events, 'professional' => $professional, 'eventsFC' => json_encode($eventsFull)];

        return $this->render($view, $return);
    }

    /**
     * @IsGranted("ROLE_CLIENT")
     * @Route("/cancel/{id}", name="_cancel", methods={"GET", "POST"})
     */
    public function cancel(Event $event, Request $request)
    {
        if ('POST' === $request->getMethod() && $request->get('reason')) {
            $client = $event->getClient();
            $professional = $event->getProfessional();

            //New event to replace the old cancelled
            $sub_event = new Event();
            $sub_event->setProfessional($professional)
                ->setStartAt($event->getStartAt())
                ->setEndAt($event->getEndAt())
                ->setTitle($event->getTitle())
                ->setDescription($event->getDescription());

            //Remove Professional from this old event that He/She just Cancelled
            $event->setProfessional(null)
                ->setValidatedAt(null)
                ->setIsAborted(true);

            $this->manager->persist($sub_event);
            $this->manager->flush();

            $email = new SendEmailMessage($client->getId(), $professional->getId(), 'canceled', true);
            $this->bus->dispatch($email);

            $message = $this->translator->trans('Your appointment was canceled successfully');
            $this->addFlash('success', $message);

            return $this->redirectToRoute('client_profile');
        }

        if ('POST' === $request->getMethod()) {
            $message = $this->translator->trans('you have to say something');
            $this->addFlash('error', $message);
        }

        return $this->render('event/cancel.html.twig', ['event' => $event, 'isPro' => true]);
    }

    /**
     * @Route("/details/{id}", name="_details", methods={"GET"})
     *
     * @return Response
     */
    public function details(Event $event)
    {
        return $this->render('professional/event/details.html.twig', ['event' => $event]);
    }

    /**
     * @Route("/restaure/{id}", name="_restaure", methods={"POST"})
     *
     * @return Response
     */
    public function restaure(Event $event)
    {
        $event->setIsFree(true)
            ->setReason(null)
            ->setValidatedAt(null)
            ->setIsAborted(false);
        $this->manager->flush();

        return $this->redirectToRoute('professional_event_index');
    }

    /**
     * @Route("/validate/{id}", name="_validate", methods={"GET"})
     *
     * @return Response
     *
     * @throws NonUniqueResultException
     */
    public function validate(Event $event)
    {
        $professional = $this->profRepo->findOneByUser($this->getUser()->getId());
        if ($professional === $event->getProfessional()) {
            $event->setValidatedAt(new DateTime());
            $this->manager->flush();

            $client_id = $event->getClient()->getId();
            $prof_id = $event->getProfessional()->getId();
            $email = new SendEmailMessage($client_id, $prof_id, 'appointment', true);
            $this->bus->dispatch($email);
        }

        return $this->redirectToRoute('professional_event_details', ['id' => $event->getId()]);
    }

    /**
     * @IsGranted("ROLE_PROFESSIONAL")
     * @Route("/delete/{id}", name="_delete", methods={"DELETE"})
     *
     * @return RedirectResponse
     */
    public function delete(Event $event, Request $request)
    {
        $message = $this->translator->trans('Delete Event failed');
        $key = 'error';
        //TODO - use token
        if ('DELETE' === $request->getMethod()) {
            if (!$event->getClient() && $event->isAnnulable()) {
                $this->manager->remove($event);
                $this->manager->flush();
                $key = 'success';
                $message = $this->translator->trans('Event deleted successfully');
            } else {
                $key = 'error';
                $message = $this->translator->trans('you can delete this event. There are Clients on');
            }
        }
        $this->addFlash($key, $message);

        return $this->redirectToRoute('professional_event_index');
    }

    /**
     * @IsGranted("ROLE_PROFESSIONAL")
     * @Route("/new", name="_create", methods={"POST"})
     * @Route("/{id}/edit", name="_edit", methods={"POST"})
     */
    public function new(Event $event = null, Request $request)
    {
        $button = $this->translator->trans('Edit');
        $message = $this->translator->trans('Event was edited successfully');
        if (!$event) {
            $event = new Event();
            $button = $this->translator->trans('Save');
            $message = $this->translator->trans('Your appointment was created. Check it in your profile appointments');
        }
        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);
        $professional = $this->profRepo->findOneByUser($this->getUser()->getId());

        //Hydration
        $event->setTitle($request->get('title'))
            ->setStartAt(new DateTime($request->get('startAt').$request->get('startAtTime')))
            ->setEndAt(new DateTime($request->get('endAt').$request->get('endAtTime')))
            ->setDescription($request->get('description'))
            ->setProfessional($professional);
        if ('Save' === $button) {
            $this->manager->persist($event);
        }
        $this->addFlash('success', $message);
        if ('POST' === $request->getMethod()) {
            $event->setModifiedAt(new DateTime());
        }
        $this->manager->flush();

        //Redirect to client appointements view
        return $this->redirectToRoute('professional_event_index');
    }

    /**
     * @IsGranted("ROLE_PROFESSIONAL")
     * @Route("/{id}/editJson", name="_editJson", methods={"POST"})
     */
    public function editJson(Event $event, Request $request)
    {
        //Hydratation - Modify only times
        $datas = json_decode($request->getContent());
        if ($datas) {
            $event->setStartAt(new DateTime(str_replace('T', ' ', $datas->start)))
                ->setEndAt(new DateTime(str_replace('T', ' ', $datas->end)));
            $event->setModifiedAt(new DateTime());
        }
        $message['message'] = $this->translator->trans('Modified successfully');
        $message['status'] = Response::HTTP_ACCEPTED;
        $this->manager->flush();

        //Redirect to client appointements view
        return new Response(json_encode($message), $message['status']);
    }

    public static function eventToJson(Event $event, bool $isClient = false): array
    {
        $events_json = [];
        if ($event) {
            $events_json = [
                'id' => $event->getId(),
                'start' => $event->getStartAt()->format('Y-m-d H:i'),
                'end' => $event->getEndAt()->format('Y-m-d H:i'),
                'backgroundColor' => $event->getIsFree() ? '#33B5E5' : '#0f0',
            ];
            if (!$isClient) {
                $events_json['title'] = $event->getTitle();
                $events_json['description'] = $event->getDescription();
            }
        }

        return $events_json;
    }
}
