<?php

namespace App\Controller\Professional;

use App\Entity\Diploma;
use App\Form\DiplomaType;
use App\Repository\DiplomaRepository as DiplomaRepo;
use App\Repository\ProfessionalRepository as ProfRepo;
use Doctrine\ORM\EntityManagerInterface as Em;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/professional/diploma", name="professional_diploma")
 * Class DiplomaController
 */
class DiplomaController extends AbstractController
{
    private TranslatorInterface $translator;
    private DiplomaRepo $repository;
    private ProfRepo $professionalRepository;
    private Em $manager;

    public function __construct(TranslatorInterface $translator, DiplomaRepo $repository, ProfRepo $profRepo, Em $em)
    {
        $this->translator = $translator;
        $this->repository = $repository;
        $this->professionalRepository = $profRepo;
        $this->manager = $em;
    }

    /**
     * @Route("/new", name="_new", methods={"GET", "POST"})
     * @Route("/{id}/edit", name="_edit", methods={"GET", "POST"})
     */
    public function new(Diploma $Diploma = null, Request $request)
    {
        $button = $this->translator->trans('Edit');
        $message = $this->translator->trans('Diploma was edited successfully');
        if (!$Diploma) {
            $Diploma = new Diploma();
            $button = $this->translator->trans('Save');
            $message = $this->translator->trans('Diploma was created successfully');
        }

        $form = $this->createForm(DiplomaType::class, $Diploma);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ('Save' === $button) {
                $prof = $this->getUser()->getClient();
                $Diploma->addProfessional($prof);
                $this->manager->persist($Diploma);
            }

            $this->manager->flush();
            $this->addFlash('success', $message);

            return $this->redirectToRoute('professional_profile');
        }
        $view = 'professional/diploma/register_notify.html.twig';

        return $this->render($view, ['form' => $form->createView(), 'button' => $button]);
    }

    /**
     * @Route("/{id}/delete", name="_delete", methods={"DELETE"})
     *
     * @return RedirectResponse
     */
    public function delete(Diploma $Diploma, Request $request)
    {
        $message = $this->translator->trans('Delete Diploma failed');
        $key = 'error';
        if ($this->isCsrfTokenValid('delete'.$Diploma->getId(), $request->get('_token'))) {
            $this->manager->remove($Diploma);
            $this->manager->flush();
            $key = 'success';
            $message = $this->translator->trans('Diploma deleted successfully');
        }

        $this->addFlash($key, $message);

        return $this->redirectToRoute('professional_profile');
    }
}
