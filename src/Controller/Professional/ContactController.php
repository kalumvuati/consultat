<?php

namespace App\Controller\Professional;

use App\Entity\Contact;
use App\Entity\Professional;
use App\Form\ContactType;
use App\Repository\ContactRepository;
use App\Repository\ProfessionalRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface as Translator;

/**
 * @Route("/contact", name="contact")
 */
class ContactController extends AbstractController
{
    private Translator $translator;
    private ObjectManager $manager;
    /**
     * @var ContactRepository|ObjectRepository
     */
    private $contactRepository;
    /**
     * @var ProfessionalRepository|ObjectRepository
     */
    private $professionalRepository;

    /**
     * ContactController constructor.
     */
    public function __construct(EntityManagerInterface $manager, Translator $translator)
    {
        $this->manager = $manager;
        $this->contactRepository = $this->manager->getRepository(Contact::class);
        $this->professionalRepository = $this->manager->getRepository(Professional::class);
        $this->translator = $translator;
    }

    /**
     * @Route("/new", name="_new", methods={"GET", "POST"})
     * @Route("/{slug}/edit", name="_edit", methods={"GET", "POST"})
     */
    public function new(Contact $contact = null, Request $request)
    {
        $button = $this->translator->trans('Edit');
        $message = $this->translator->trans('Contact was edited successfully');
        if (!$contact) {
            $contact = new Contact();
            $button = $this->translator->trans('Save');
            $message = $this->translator->trans('Contact was created successfully');
        }

        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ('Save' === $button) {
                $prof = $this->getUser()->getClient();
                $contact->setProfessional($prof);
                $this->manager->persist($contact);
            }

            $this->manager->flush();
            $this->addFlash('success', $message);

            return $this->redirectToRoute('professional_profile');
        }
        $view = 'professional/contact/register_notify.html.twig';

        return $this->render($view, ['form' => $form->createView(), 'button' => $button]);
    }

    /**
     * @Route("/{slug}/delete", name="_delete", methods={"DELETE"})
     *
     * @return RedirectResponse
     */
    public function delete(Contact $contact, Request $request)
    {
        $message = $this->translator->trans('Delete contact failed');
        $key = 'error';
        if ($this->isCsrfTokenValid('delete'.$contact->getSlug(), $request->get('_token'))) {
            $this->manager->remove($contact);
            $this->manager->flush();
            $key = 'success';
            $message = $this->translator->trans('Contact deleted successfully');
        }

        $this->addFlash($key, $message);

        return $this->redirectToRoute('professional_profile');
    }
}
