<?php

namespace App\Controller\Professional;

use App\Entity\Domaine;
use App\Form\DomaineType;
use App\Repository\DomaineRepository as DomaineRepo;
use App\Repository\ProfessionalRepository as ProfRepo;
use Doctrine\ORM\EntityManagerInterface as Em;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/professional/domaine", name="professional_domaine")
 * Class DomaineController
 */
class DomaineController extends AbstractController
{
    private TranslatorInterface $translator;
    private DomaineRepo $repository;
    private ProfRepo $professionalRepository;
    private Em $manager;

    public function __construct(TranslatorInterface $translator, DomaineRepo $repository, ProfRepo $profRepo, Em $em)
    {
        $this->translator = $translator;
        $this->repository = $repository;
        $this->professionalRepository = $profRepo;
        $this->manager = $em;
    }

    /**
     * @Route("/new", name="_new", methods={"GET", "POST"})
     * @Route("/{slug}/edit", name="_edit", methods={"GET", "POST"})
     */
    public function new(Domaine $domaine = null, Request $request)
    {
        $button = $this->translator->trans('Edit');
        $message = $this->translator->trans('Domaine was edited successfully');
        if (!$domaine) {
            $domaine = new Domaine();
            $button = $this->translator->trans('Save');
            $message = $this->translator->trans('Domaine was created successfully');
        }

        $form = $this->createForm(DomaineType::class, $domaine);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ('Save' === $button) {
                $domaine->addProfessional($this->getUser()->getClient());
                $this->manager->persist($domaine);
            }

            $this->manager->flush();
            $this->addFlash('success', $message);

            return $this->redirectToRoute('professional_profile');
        }
        $view = 'professional/contact/register_notify.html.twig';

        return $this->render($view, ['form' => $form->createView(), 'button' => $button]);
    }

    /**
     * @Route("/{slug}/delete", name="_delete", methods={"DELETE"})
     *
     * @return RedirectResponse
     */
    public function delete(Domaine $domaine, Request $request)
    {
        $message = $this->translator->trans('Delete domaine failed');
        $key = 'error';
        if ($this->isCsrfTokenValid('delete'.$domaine->getSlug(), $request->get('_token'))) {
            $this->manager->remove($domaine);
            $this->manager->flush();
            $key = 'success';
            $message = $this->translator->trans('Domaine deleted successfully');
        }

        $this->addFlash($key, $message);

        return $this->redirectToRoute('professional_profile');
    }
}
