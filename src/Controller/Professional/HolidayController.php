<?php

namespace App\Controller\Professional;

use App\Entity\Holiday;
use App\Form\HolidayType;
use App\Repository\HolidayRepository as HolidayRepo;
use App\Repository\ProfessionalRepository as ProfRepo;
use Doctrine\ORM\EntityManagerInterface as Em;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/professional/holiday", name="professional_holiday")
 * Class HolidayController
 */
class HolidayController extends AbstractController
{
    private TranslatorInterface $translator;
    private HolidayRepo $repository;
    private ProfRepo $professionalRepository;
    private Em $manager;

    public function __construct(TranslatorInterface $translator, HolidayRepo $repository, ProfRepo $profRepo, Em $em)
    {
        $this->translator = $translator;
        $this->repository = $repository;
        $this->professionalRepository = $profRepo;
        $this->manager = $em;
    }

    /**
     * @Route("/new", name="_new", methods={"GET", "POST"})
     * @Route("/{id}/edit", name="_edit", methods={"GET", "POST"})
     */
    public function new(Holiday $holiday = null, Request $request)
    {
        $button = $this->translator->trans('Edit');
        $message = $this->translator->trans('Holiday was edited successfully');
        if (!$holiday) {
            $holiday = new Holiday();
            $button = $this->translator->trans('Save');
            $message = $this->translator->trans('Holiday was created successfully');
        }

        $form = $this->createForm(HolidayType::class, $holiday);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ('Save' === $button) {
                $holiday->addProfessional($this->getUser()->getClient());
                $this->manager->persist($holiday);
            }

            $this->manager->flush();
            $this->addFlash('success', $message);

            return $this->redirectToRoute('professional_profile');
        }
        $view = 'professional/holiday/register_notify.html.twig';

        return $this->render($view, ['form' => $form->createView(), 'button' => $button]);
    }

    /**
     * @Route("/{id}/delete", name="_delete", methods={"DELETE"})
     *
     * @return RedirectResponse
     */
    public function delete(Holiday $holiday, Request $request)
    {
        $message = $this->translator->trans('Delete Holiday failed');
        $key = 'error';
        if ($this->isCsrfTokenValid('delete'.$holiday->getId(), $request->get('_token'))) {
            $this->manager->remove($holiday);
            $this->manager->flush();
            $key = 'success';
            $message = $this->translator->trans('Holiday deleted successfully');
        }

        $this->addFlash($key, $message);

        return $this->redirectToRoute('professional_profile');
    }
}
