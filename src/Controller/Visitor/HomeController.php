<?php

namespace App\Controller\Visitor;

use App\Entity\Address;
use App\Entity\Domaine;
use App\Repository\ClientRepository;
use App\Repository\ProfessionalRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    private ProfessionalRepository $repository;
    private ClientRepository $client;

    public function __construct(ProfessionalRepository $repository, ClientRepository $client)
    {
        $this->repository = $repository;
        $this->client = $client;
    }

    /**
     * @Route("/", name="home")
     */
    public function index(Request $request): Response
    {
        $professionals = [];
        $query = $request->query->all();

        if (isset($query) && !empty($query)) {
            //Preparation of search arguments
            $query = $this->removeEmptyIndex($query);

            //This method can be refactored and optimise
            if (isset($query['domaine'])) {
                $size = strlen($query['domaine']);
                $start = substr($query['domaine'], 0, $size / 2);
                $end = substr($query['domaine'], $size / 2, $size);
                $domaineRepo = $this->getDoctrine()->getRepository(Domaine::class);
                $domaines = $domaineRepo->findByDomaine([$start, $end]);
                /** @var Domaine $domaine */
                foreach ($domaines as $domaine) {
                    $profs = $domaine->getProfessionals();
                    foreach ($profs as $prof) {
                        //Only the Actif Professional will be visible
                        if ($prof->getIsActif()) {
                            $professionals[] = $prof;
                        }
                    }
                }
            }
            //Location
            if (isset($query['location'])) {
                $addressRepo = $this->getDoctrine()->getRepository(Address::class);
                $addresses = $addressRepo->findByAddressInfo($query['location']);
                /* @var Address $domaine */
                foreach ($addresses as $address) {
                    $profs = $address->getProfessionals();
                    foreach ($profs as $prof) {
                        //Only the Actif Professional will be visible
                        if ($prof->getIsActif()) {
                            $professionals[] = $prof;
                        }
                    }
                }
            }
        }
        return $this->render('home/index.html.twig', ['professionals' => $professionals]);
    }

    private function removeEmptyIndex(array $params)
    {
        $params_temp = [];
        foreach ($params as $key => $param) {
            if (!empty($param)) {
                $params_temp[$key] = $param;
            }
        }

        return $params_temp;
    }
}
