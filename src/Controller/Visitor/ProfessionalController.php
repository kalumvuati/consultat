<?php

namespace App\Controller\Visitor;

use App\Controller\Professional\EventController;
use App\Entity\Professional;
use App\Repository\EventRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityManagerInterface as Manager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/visitor/professional", name="visitor_professional")
 */
class ProfessionalController extends AbstractController
{
    private Manager $manager;
    private EventRepository $repository;

    public function __construct(EntityManagerInterface $manager, EventRepository $repository)
    {
        $this->manager = $manager;
        $this->repository = $repository;
    }

    /**
     * Professional timetable and more details
     * Visible by every visitor Details = Profile.
     *
     * @Route("/profile/{slug}", name="_profile", methods={"GET"})
     */
    public function profile(Professional $professional)
    {
        $events = $this->repository->findBy(['professional' => $professional->getId()]);
        $events_json = [];
        foreach ($events as $event) {
            $events_json[] = EventController::eventToJson($event, true);
        }
        $view = 'visitor/professional/profile.html.twig';
        $return = ['events' => json_encode($events_json), 'professional' => $professional];

        return $this->render($view, $return);
    }
}
