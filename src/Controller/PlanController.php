<?php

namespace App\Controller;

use App\Entity\PaymentMethod;
use App\Entity\Plan;
use App\Entity\User;
use App\Form\PayementMethodType;
use App\Form\PlanType;
use App\Message\SendPlanTransitionMessage;
use App\Repository\PaymentMethodRepository;
use App\Repository\PlanRepository as PlanRepo;
use App\Repository\ProfessionalRepository as ProfRepo;
use App\Service\StripeHelper as Stripe;
use Doctrine\ORM\EntityManagerInterface as Em;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Stripe\Exception\ApiErrorException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface as Translator;

/**
 * @Route("/plan", name="plan")
 * Class PlanController
 */
class PlanController extends AbstractController
{
    private Translator $translator;
    private PlanRepo $repository;
    private ProfRepo $professional;
    private Em $manager;
    private Stripe $stripeHelper;

    public function __construct(Translator $translator, PlanRepo $repo, ProfRepo $profRepo, Em $em, Stripe $helper)
    {
        $this->translator = $translator;
        $this->repository = $repo;
        $this->professional = $profRepo;
        $this->manager = $em;
        $this->stripeHelper = $helper;
    }

    /**
     * @Route("/plans", name="_index", methods={"GET"})
     */
    public function index()
    {
        $plans = $this->repository->findBy(['isVisible' => true]);

        return $this->render('plan/index.html.twig', ['plans' => $plans]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/new", name="_admin_new", methods={"GET", "POST"})
     * @Route("/{slug}/edit", name="_admin_edit", methods={"GET", "POST"})
     *
     * @throws ApiErrorException
     */
    public function new(Plan $plan = null, Request $request)
    {
        $button = $this->translator->trans('Edit');
        $message = $this->translator->trans('Plan was edited successfully');
        if (!$plan) {
            $plan = new Plan();
            $button = $this->translator->trans('Save');
            $message = $this->translator->trans('Plan was created successfully');
        }

        $form = $this->createForm(PlanType::class, $plan);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ('Save' === $button) {
                $this->stripeHelper->createPlan($plan);
            }

            $this->manager->flush();
            $this->addFlash('success', $message);

            return $this->redirectToRoute('professional_profile');
        }
        $view = 'admin/plan/new.html.twig';

        return $this->render($view, ['form' => $form->createView(), 'button' => $button]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/{id}/delete", name="_delete", methods={"DELETE"})
     *
     * @return RedirectResponse
     */
    public function delete(Plan $plan, Request $request, MessageBusInterface $bus)
    {
        $message = $this->translator->trans('Delete Plan failed');
        $key = 'error';
        if ($this->isCsrfTokenValid('delete'.$plan->getId(), $request->get('_token'))) {
            $this->stripeHelper->detelePlan($plan->getSubscriptionId());
            $subscribers = $plan->getProfessionnals();

            $standard_plan = $this->repository->findOneBy(['title' => 'default']);
            $professionals_id = [];
            //Remove professionals from this plan
            foreach ($subscribers as $subscriber) {
                $plan->removeProfessionnal($subscriber);
                //To a transition plan and informe the User about the change
                $subscriber->setPlan($standard_plan);
                $professionals_id[] = $subscriber->getId();
            }
            $this->manager->remove($plan);
            $this->manager->flush();
            //Informe Professional about the delete of this plan
            $msg = new SendPlanTransitionMessage($professionals_id);
            $bus->dispatch($msg);

            $key = 'success';
            $message = $this->translator->trans('Plan deleted successfully');
        }

        $this->addFlash($key, $message);

        return $this->redirectToRoute('professional_profile');
    }

    /**
     * @IsGranted("ROLE_PROFESSIONAL")
     * @Route("/{slug}/payment_method", name="_payment_method", methods={"GET", "POST"})
     */
    public function paymentMethod(Plan $plan, Request $request, PaymentMethodRepository $repository)
    {
        $paymentMethod = new PaymentMethod();
        $form = $this->createForm(PayementMethodType::class, $paymentMethod);
        $form->handleRequest($request);
        /** @var User $user */
        $user = $this->getUser();
        $professional = $this->professional->findOneByUser($user->getId());
        if ($form->isSubmitted() && $form->isValid()) {
            //Stripe Payment method
            if ($customer = $this->stripeHelper->createCustomer($professional, $paymentMethod)) {
                $professional->setCustomerId($customer->id);
                $paymentMethod->setProfessional($professional);
                $this->manager->persist($paymentMethod);
                $this->manager->flush();
                $message = $this->translator->trans('Professional profile modified successfully');
                $this->addFlash('success', $message);
            } else {
                $this->addFlash('error', $this->stripeHelper->getMessage());
            }
        }
        $paymentMethods = $professional->getPaymentMethods();
        if (0 === $professional->getPaymentMethods()->count()) {
            $paymentMethods = null;
        }

        $view = 'professional/plan/paymentMethod.html.twig';

        return $this->render($view, ['methods' => $paymentMethods, 'form' => $form->createView(), 'plan' => $plan]);
    }

    /**
     * @Route("/subscribe/{slug}", name="_subscribe", methods={"POST"})
     */
    public function subscribe(Plan $plan)
    {
        /** @var User $user */
        $user = $this->getUser();
        $professional = $this->professional->findOneByUser($user->getId());

        if ($professional) {
            $this->stripeHelper->subscription($professional, $plan);

            $message = 'Congratulations! You are now subscribed on '.$plan->getTitle().'plan';
            $this->addFlash('success', $this->translator->trans($message));
        } else {
            $message = 'Oops! something wrong happened during subscription. Try again later.';
            $this->addFlash('error', $this->translator->trans($message));
        }

        return $this->redirectToRoute('professional_profile');
    }

    /**
     * @Route("/unsubscribe/{slug}", name="_unsubscribe", methods={"GET"})
     */
    public function unSubscribe(Plan $plan)
    {
        /** @var User $user */
        $user = $this->getUser();
        $professional = $this->professional->findOneByUser($user->getId());

        if ($professional && $professional->getSubscriptionStripeId()) {
            $message = $this->stripeHelper->unSubscription($professional->getSubscriptionStripeId());
            $professional->setSubscriptionStripeId(null);
            $plan->removeProfessionnal($professional);
            $message .= ' Congratulations! You are now Unsubscribed from '.$plan->getTitle().'plan';
            $this->addFlash('success', $this->translator->trans($message));
            $this->manager->flush();
        } else {
            $message = "Oops! We couldn't unsubscribe you from this plan. Try again later.";
            $this->addFlash('error', $this->translator->trans($message));
        }

        return $this->redirectToRoute('professional_profile');
    }
}
