<?php

namespace App\Controller\Address;

use App\Entity\Address;
use App\Form\AddressType;
use App\Repository\AddressRepository;
use App\Repository\ProfessionalRepository;
use Doctrine\ORM\EntityManagerInterface as Manager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface as Trans;

/**
 * @Route("/address", name="address")
 */
class AddressController extends AbstractController
{
    private AddressRepository $repository;
    private Manager $manager;
    private Trans $translator;

    public function __construct(AddressRepository $repository, Manager $manager, Trans $translator)
    {
        $this->repository = $repository;
        $this->manager = $manager;
        $this->translator = $translator;
    }

    /**
     * @Route("/new", name="_new", methods={"GET", "POST"})
     * @Route("/{id}/edit", name="_edit", methods={"GET", "POST"})
     *
     * @return Response
     */
    public function new(Address $address = null, Request $request, ProfessionalRepository $repository)
    {
        $button = $this->translator->trans('Edit');
        $message = $this->translator->trans('Address modified successfully');
        if (!$address) {
            $address = new Address();
            $button = $this->translator->trans('Save');
            $message = $this->translator->trans('Address created successfully');
        }

        $form = $this->createForm(AddressType::class, $address);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //Check if this address exist
            $url = 'https://api-adresse.data.gouv.fr/search/?q='.$address->getAddress();
            $url .= ', '.$address->getZipcode().', '.$address->getCity();
            $options = [];
            $response = HttpClient::create()->request('GET', $url, $options);
            try {
                $features = json_decode($response->getContent())->features;
            } catch (ClientExceptionInterface | RedirectionExceptionInterface  $e) {
                $message = $e->getMessage();
            } catch (ServerExceptionInterface | TransportExceptionInterface $e) {
                $message = $e->getMessage();
            }
            $found = false;
            if (1 === count($features)) {
                $found = true;
            }
            foreach ($features as $feature) {
                if (strtolower($feature->properties->name) === strtolower($address->getAddress())) {
                    $found = true;
                    break;
                    //TODO get x, y and longitude and altitude for GooglePlace ou Map
                }
            }
            if ($found) {
                if ('Save' === $button) {
                    $prof = $this->getUser()->getClient();
                    $address->addProfessional($prof);
                    $this->manager->persist($address);
                }
                $this->manager->flush();

                $this->addFlash('success', $message);
            } else {
                $message = $this->translator->trans('This address is not well formatted');
                $this->addFlash('error', $message);
            }

            return $this->redirectToRoute('professional_profile');
        }

        return $this->render('address/new.html.twig', ['form' => $form->createView(), 'button' => $button]);
    }

    /**
     * @Route("/{id}/delete", name="_delete", methods={"DELETE"})
     *
     * @return RedirectResponse
     */
    public function delete(Address $address, Request $request)
    {
        $message = $this->translator->trans('Delete address failed');
        $key = 'error';
        if ($this->isCsrfTokenValid('delete'.$address->getId(), $request->get('_token'))) {
            $this->manager->remove($address);
            $this->manager->flush();
            $key = 'success';
            $message = $this->translator->trans('Address deleted successfully');
        }

        $this->addFlash($key, $message);

        return $this->redirectToRoute('professional_profile');
    }
}
