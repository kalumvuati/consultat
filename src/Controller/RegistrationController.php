<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Professional;
use App\Entity\User;
use App\Form\ClientFormType;
use App\Form\ProfessionalFormType;
use App\Form\UserFormType;
use App\Message\NewUserWelcome;
use App\Repository\UserRepository;
use App\Security\EmailVerifier;
use App\Security\UserAuthenticator as Auth;
use App\Service\SiretHelper;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface as Encoder;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler as Guard;
use Symfony\Contracts\Translation\TranslatorInterface as Translator;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

class RegistrationController extends AbstractController
{
    private $emailVerifier;
    private Translator $translator;
    private MessageBusInterface $bus;

    public function __construct(EmailVerifier $emailVerifier, Translator $translator, MessageBusInterface $bus)
    {
        $this->emailVerifier = $emailVerifier;
        $this->translator = $translator;
        $this->bus = $bus;
    }

    /**
     * @Route("/register", name="app_register")
     */
    public function registerAsClient(Request $request, Encoder $Encoder, Guard $guardHandler, Auth $auth): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('client_profile');
        }

        $user = new User();
        $client = new Client();
        $formUser = $this->createForm(UserFormType::class, $user);
        $formClient = $this->createForm(ClientFormType::class, $client);
        $formUser->handleRequest($request);
        $formClient->handleRequest($request);
        if ($formUser->isSubmitted() && $formUser->isValid() && $formClient->isSubmitted() && $formClient->isValid()) {
            // encode the plain password
            $user->setPassword($Encoder->encodePassword($user, $formUser->get('plainPassword')->getData()));
            $user->setRoles(['ROLE_CLIENT']);
            $entityManager = $this->getDoctrine()->getManager();
            $user->setClient($client);
            $entityManager->persist($user);
            $client->setUser($user);
            $entityManager->persist($client);
            $entityManager->flush();

            //Message Notification
            $message = new NewUserWelcome($user->getId());
            $this->bus->dispatch($message);

            //Native
            $from = new Address('service@domain.ext', 'Service Mail');
            $subject = $this->translator->trans('Please Confirm your Email');
            $template = 'registration/confirmation_email.html.twig';
            $templateInst = (new TemplatedEmail())->from($from)->to($user->getEmail())->subject($subject);
            $templateInst->htmlTemplate($template);
            $this->emailVerifier->sendEmailConfirmation('app_verify_email', $user, $templateInst);
            // do anything else you need here, like send an email
            // firewall name in security.yaml
            return $guardHandler->authenticateUserAndHandleSuccess($user, $request, $auth, 'main');
        }
        $returnArray = ['UserForm' => $formUser->createView(), 'ClientForm' => $formClient->createView()];

        return $this->render('registration/client.html.twig', $returnArray);
    }

    /**
     * @Route("/register_professional", name="app_register_professional")
     */
    public function registerAsProfessional(Request $request, Encoder $Encoder, Guard $guard, Auth $auth): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }

        $user = new User();
        $professional = new Professional();

        $formUser = $this->createForm(UserFormType::class, $user);
        $formProf = $this->createForm(ProfessionalFormType::class, $professional);

        $formUser->handleRequest($request);
        $formProf->handleRequest($request);
        $siret = '';
        $message = $key = '';
        if ($formUser->isSubmitted() && $formUser->isValid() && $formProf->isSubmitted() && $formProf->isValid()) {
            //check SIRET
            if ($siret = $request->get('siret')) {
                // encode the plain password
                $user->setPassword($Encoder->encodePassword($user, $formUser->get('plainPassword')->getData()));
                $user->setRoles(['ROLE_PROFESSIONAL']);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $professional->setUser($user);

                $professional->setSiret($request->get('siret'));
                $entityManager->persist($professional);

                $entityManager->flush();

                $from = new Address('service@domain.ext', 'Service Mail');
                $subject = 'Please Confirm your Email';
                $template = 'registration/confirmation_email.html.twig';
                $templateInst = (new TemplatedEmail())->from($from)->to($user->getEmail())->subject($subject);
                $templateInst->htmlTemplate($template);
                $this->emailVerifier->sendEmailConfirmation('app_verify_email', $user, $templateInst);

                // do anything else you need here, like send an email
                // firewall name in security.yaml
                return $guard->authenticateUserAndHandleSuccess($user, $request, $auth, 'main');
            } else {
                $key = 'error';
                $message = $this->translator->trans('for finish this subscription, SIRET is necessary');
            }
        }

        //Specific for this view
        if ($user->getEmail()) {
            $siret = $request->get('siret');
            $this->addFlash($key, $message);
        }

        $returnArr = ['UserForm' => $formUser->createView(), 'ProfForm' => $formProf->createView(), 'siret' => $siret];

        return $this->render('registration/professional.html.twig', $returnArr);
    }

    /**
     * @Route("/siret_checker", name="siret_checker", methods={"POST"})
     *
     * @return Response
     *
     * @throws NonUniqueResultException
     */
    public function siretChecker(Request $request, SiretHelper $siretHelper)
    {
        $content = $request->request->all();
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $entityManager->getRepository(Professional::class);
        $response_data = ['message' => $this->translator->trans('We cannot found siret code anymore')];
        $status = Response::HTTP_NOT_FOUND;
        //Check if this professional is already exit
        if (isset($content['siret'])) {
            $prof = $repository->findOneBySiret($content['siret']);
            if ($prof) {
                $response_data = ['message' => $this->translator->trans('This siret exists, try an other')];
                $status = Response::HTTP_ALREADY_REPORTED;
            } else {
                $response_data = $siretHelper->checkSiret($content['siret']);
                if (isset($response_data['siret'])) {
                    $status = Response::HTTP_OK;
                } else {
                    $status = Response::HTTP_NOT_FOUND;
                }
            }
        }
        $response_data['status'] = $status;
        return new Response(json_encode($response_data), $status, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("/verify/email", name="app_verify_email")
     */
    public function verifyUserEmail(Request $request, UserRepository $userRepository): Response
    {
        $id = $request->get('id');

        if (null === $id) {
            return $this->redirectToRoute('app_register');
        }

        $user = $userRepository->find($id);

        if (null === $user) {
            return $this->redirectToRoute('app_register');
        }

        // validate email confirmation link, sets User::isVerified=true and persists
        try {
            $this->emailVerifier->handleEmailConfirmation($request, $user);
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('verify_email_error', $exception->getReason());

            return $this->redirectToRoute('app_register');
        }

        $message = $this->translator->trans('Your email address has been verified.');
        $this->addFlash('success', $message);

        return $this->redirectToRoute('app_register');
    }
}
