<?php

namespace App\Controller\Client;

use App\Entity\Client;
use App\Entity\Event;
use App\Entity\Professional;
use App\Entity\User;
use App\Message\SendEmailMessage;
use App\Repository\EventRepository;
use App\Repository\ProfessionalRepository;
use Doctrine\ORM\EntityManagerInterface as Em;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface as Translator;

/**
 * @Route("/client/event", name="client_event")
 */
class EventController extends AbstractController
{
    private Translator $translator;
    private ObjectManager $manager;
    private ProfessionalRepository $profRepo;
    private MessageBusInterface $bus;

    /**
     * EventController constructor.
     */
    public function __construct(Em $manager, Translator $trans, MessageBusInterface $bus)
    {
        $this->manager = $manager;
        $this->profRepo = $this->manager->getRepository(Professional::class);
        $this->translator = $trans;
        $this->bus = $bus;
    }

    /**
     * @Route("/index", name="_index", methods={"GET"})
     */
    public function index(): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $professional = $this->profRepo->findOneByUser($user->getId());
        $view = 'client/event/index.html.twig';
        $events = $user->getClient()->getEvents();
        $eventsFull = [];

        //Convert php event to Json
        foreach ($events as $event) {
            $eventsFull[] = self::eventToJson($event);
        }
        $return = ['events' => $events, 'client' => $professional, 'eventsFC' => json_encode($eventsFull)];

        return $this->render($view, $return);
    }

    /**
     * @Route("/cancel/{id}", name="_cancel", methods={"GET", "POST"})
     */
    public function cancel(Event $event, Request $request)
    {
        if ('POST' === $request->getMethod() && $request->get('reason')) {
            $client = $event->getClient();
            $professional = $event->getProfessional();

            //Check if client have to pay this event before the cancellation
            if ($event && $event->isAnnulable()) {
                //Remove Client from this old event that He/She just Cancelled
                $event->setClient(null)
                    ->setValidatedAt(null)
                    ->setIsAborted(true)
                    ->setReason($request->get('reason'));
                if ($professional) {
                    //when event is not canceled by professional or where Professional is present on it
                    $prof_id = $professional->getId(); // did like that because of PHPCS
                    $email = new SendEmailMessage($client->getId(), $prof_id, 'canceled', true);
                    $this->bus->dispatch($email);
                }

                $this->manager->flush();

                $message = $this->translator->trans('Your appointment was canceled successfully');
                $this->addFlash('success', $message);

                return $this->redirectToRoute('client_profile');
            } else {
                $message = 'This event is validated and hours left are under 24 hours.';
                $message .= ' Please, contact this Professional to solve this problem accord terms.';
                $this->addFlash('error', $message);
                $message = $this->translator->trans($message);
            }
        }

        if ('POST' === $request->getMethod() && !$message) {
            $message = $this->translator->trans('you have to say something');
            $this->addFlash('error', $message);
        }

        return $this->render('client/event/cancel.html.twig', ['event' => $event, 'isClient' => true]);
    }

    /**
     * @Route("/appointment/{slug}", name="_appointment", methods={"GET", "POST"})
     */
    public function appointment(Professional $professional, Request $request, EventRepository $repository)
    {
        $criteria = ['professional' => $professional->getId(), 'isFree' => true];
        $events = $repository->findBy($criteria, ['start_at' => 'ASC']);
        $token = $this->isCsrfTokenValid('appointment'.$professional->getSlug(), $request->get('_token'));
        if ('POST' === $request->getMethod() && $token) {
            $decision = $request->get('event') && $request->get('reason');
            if ($decision && ($event = $repository->find($request->get('event')))) {
                /** @var Client $client */
                $client = $this->getUser()->getClient();
                $event->setClient($client);
                $event->setIsFree(false);
                //Dispatch message
                $msg = new SendEmailMessage($client->getId(), $professional->getId(), 'appointment');
                $this->bus->dispatch($msg);

                $this->manager->flush();

                $message = $this->translator->trans('Your appointment was send to Professional successfully');
                $this->addFlash('success', $message);

                return $this->redirectToRoute('client_event_index');
            } else {
                $message = 'We found some error. Please make sure to select time and to give motivation';
                $message = $this->translator->trans($message);
                $this->addFlash('warning', $message);
            }
        }
        $return = ['events' => $events, 'professional' => $professional];

        return $this->render('client/event/appointment.html.twig', $return);
    }

    /**
     * @Route("/demove/{id}", name="_remove", methods={"DELETE"})
     *
     * @return RedirectResponse
     */
    public function remove(Event $event, Request $request)
    {
        $message = $this->translator->trans('Delete Event failed');
        $key = 'error';
        $conditions = $this->isCsrfTokenValid('delete'.$event->getId(), $request->get('_token'));
        if ($conditions && !$event->getProfessional()) {
            $this->manager->remove($event);
            $key = 'success';
            $message = $this->translator->trans('Event deleted successfully');
        } elseif ($conditions && $event->getProfessional()) {
            $event->setClient(null);
        }
        $this->manager->flush();
        $this->addFlash($key, $message);

        return $this->redirectToRoute('client_event_index');
    }

    public static function eventToJson(Event $event): array
    {
        $events_json = [];
        if ($event) {
            $events_json = [
                'id' => $event->getId(),
                'title' => $event->getTitle(),
                'start' => $event->getStartAt()->format('Y-m-d H:i'),
                'end' => $event->getEndAt()->format('Y-m-d H:i'),
                'description' => $event->getDescription(),
                'backgroundColor' => '#33B5E5',
            ];
        }

        return $events_json;
    }
}
