<?php

namespace App\Controller\Client;

use App\Entity\Client;
use App\Entity\User;
use App\Form\ClientType;
use App\Repository\ClientRepository;
use Doctrine\ORM\EntityManagerInterface as Em;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface as Translator;

/**
 * @Route("/client", name="client")
 */
class ClientController extends AbstractController
{
    private ClientRepository $repository;
    private Em $manager;
    private Translator $translator;

    public function __construct(ClientRepository $repository, Em $manager, Translator $translator)
    {
        $this->repository = $repository;
        $this->manager = $manager;
        $this->translator = $translator;
    }

    /**
     * @Route("/profile", name="_profile", methods={"GET", "POST"})
     */
    public function profile()
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($this->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('admin_profile');
        } elseif ($this->isGranted('ROLE_PROFESSIONAL')) {
            return $this->redirectToRoute('professional_profile');
        }

        return $this->render('client/profile.html.twig', ['client' => $user->getClient()]);
    }

    /**
     * @Route("/profile/edit", name="_profile_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request)
    {
        $client = $this->getUser()->getClient();
        $form = $this->createForm(ClientType::class, $client);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //Hydratation - Address
            $this->manager->flush();
            $message = $this->translator->trans('Professional profile modified successfully');
            $this->addFlash('success', $message);

            return $this->redirectToRoute('client_profile');
        }
        $return = ['client' => $client, 'form' => $form->createView()];

        return $this->render('client/edit.html.twig', $return);
    }
}
