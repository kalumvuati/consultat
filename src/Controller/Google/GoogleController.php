<?php

namespace App\Controller\Google;

use App\Entity\Event;
use App\Form\EventType;
use App\Repository\ProfessionalRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface as Em;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class GoogleController extends AbstractController
{
    private \Google_Client $client;
    private $url_prefix;
    private ProfessionalRepository $repository;
    private Em $manager;
    private TranslatorInterface $translator;

    public function __construct(\Google_Client $client, TranslatorInterface $translator, Em $manager)
    {
        $this->client = $client;
        $this->client->setScopes(\Google_Service_Calendar::CALENDAR_READONLY);
        $redirect_uri = 'https://'.$_SERVER['HTTP_HOST'].'/check';
        $this->client->setRedirectUri($redirect_uri);
        $this->url_prefix = 'https://www.googleapis.com/calendar/v3/';
        $this->manager = $manager;
        $this->translator = $translator;
    }

    /**
     * @Route("/calendar", name="calendar")
     */
    public function index(): Response
    {
        return $this->redirect($this->client->createAuthUrl());
    }

    /**
     * @Route("/check", name="check")
     */
    public function check(Request $request): Response
    {
        if (($code = $request->get('code'))) {
            $request->getSession()->set('code', $code);

            //Getting access token
            try {
                $access_token = $this->client->fetchAccessTokenWithAuthCode($code);
                $request->getSession()->set('access_token', $access_token);
                $url_middle = 'users/me/calendarList';
                $data = $this->getResponse($request, $url_middle);
            } catch (\Exception $exception) {
                return $this->redirectToRoute('calendar');
//                throw new NotFoundHttpException('You must reload you initial page to get a new access token');
            }
        }
        $calendars = [];
        if ($data) {
            foreach ($data->items as $item) {
                if ('owner' === $item->accessRole) {
                    $calendars[] = [
                        'id' => $item->id,
                        'summary' => $item->summary,
                    ];
                }
            }
        }

        return $this->render('client/google/calendars_list.html.twig', ['calendars' => $calendars]);
    }

    /**
     * @Route("/calendar_event/{calendar_id}", name="calendar_event", methods={"GET"})
     */
    public function calendarEvents(string $calendar_id, Request $request, ProfessionalRepository $repository): Response
    {
        $url_middle = 'calendars/'.$calendar_id.'/events';
        $data = $this->getResponse($request, $url_middle);
        if ($data) {
            $professional = $repository->findOneByUser($this->getUser()->getId());
            $event_number = 0;
            foreach ($data->items as $item) {
                $start_at = $item->start->dateTime ?? '';
                $end_at = $item->end->dateTime ?? '';
                //TODO - only event from today and remove duplicated event
                if ($start_at && $end_at) {
                    $event = (new Event())
                    ->setProfessional($professional)
                    ->setTitle($item->summary)
                    ->setDescription($item->description ?? '')
                    ->setStartAt(new DateTime($start_at))
                    ->setEndAt(new DateTime($end_at));
                    $this->manager->persist($event);
                    ++$event_number;
                }
            }
            $this->manager->flush();
            //TODO to adapte message plural/singular according events_number
            $message = $this->translator->trans($event_number.' Event(s) where added successfully');
            $this->addFlash('success', $message);
        }

        return $this->redirectToRoute('client_profile');
    }

    private function getResponse(Request $request, string $url_middle)
    {
        $access_token = $request->getSession()->get('access_token');
        $response = null;
        if ($access_token) {
            //Getting access token
            $calendar_id = $request->get('calendar_id');
            $client_guzzle = HttpClient::create();
            $url = $this->url_prefix.$url_middle.'?key='.$this->client->getConfig('developer_key');
            $options = [
                'headers' => [
                    'Authorization' => 'Bearer '.$access_token['access_token'],
                    'Accept' => 'application/json',
                ],
            ];

            try {
                $response = $client_guzzle->request('GET', $url, $options);
            } catch (\Exception $exception) {
                throw new NotFoundHttpException('GET Request not found. Reason : '.$exception->getMessage());
            }
        }
        if ($response instanceof ResponseInterface) {
            return json_decode($response->getContent());
        }

        return null;
    }
}
