<?php

namespace App\Controller\Admin;

use App\Entity\Interval;
use App\Entity\Professional;
use App\Form\IntervalType;
use App\Repository\IntervalRepository;
use App\Repository\ProfessionalRepository;
use Doctrine\ORM\EntityManagerInterface as Em;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface as Trans;

/**
 * @Route("/admin/interval", name="admin_interval")
 */
class IntervalController extends AbstractController
{
    private Trans $translator;
    private Em $manager;
    /**
     * @var IntervalRepository|ObjectRepository
     */
    private $intervalRepository;
    /**
     * @var ProfessionalRepository|ObjectRepository
     */
    private $professionalRepository;

    /**
     * IntervalController constructor.
     */
    public function __construct(Em $manager, Trans $translator)
    {
        $this->manager = $manager;
        $this->intervalRepository = $this->manager->getRepository(Interval::class);
        $this->professionalRepository = $this->manager->getRepository(Professional::class);
        $this->translator = $translator;
    }

    /**
     * @Route("/new", name="_new", methods={"GET", "POST"})
     * @Route("/{id}/edit", name="_edit", methods={"GET", "POST"})
     */
    public function new(Interval $interval = null, Request $request)
    {
        $button = $this->translator->trans('Edit');
        $message = $this->translator->trans('Interval was edited successfully');
        if (!$interval) {
            $interval = new Interval();
            $button = $this->translator->trans('Save');
            $message = $this->translator->trans('Interval was created successfully');
        }

        $form = $this->createForm(IntervalType::class, $interval);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ('Save' === $button) {
                $this->manager->persist($interval);
            }

            $this->manager->flush();
            $this->addFlash('success', $message);

            return $this->redirectToRoute('admin_profile');
        }
        $view = 'admin/interval/new.html.twig';

        return $this->render($view, ['form' => $form->createView(), 'button' => $button]);
    }

    /**
     * @Route("/{id}/delete", name="_delete", methods={"DELETE"})
     *
     * @return RedirectResponse
     */
    public function delete(Interval $interval, Request $request)
    {
        $message = $this->translator->trans('Delete Interval failed');
        $key = 'error';
        if ($this->isCsrfTokenValid('delete'.$interval->getId(), $request->get('_token'))) {
            $this->manager->remove($interval);
            $this->manager->flush();
            $key = 'success';
            $message = $this->translator->trans('Interval deleted successfully');
        }

        $this->addFlash($key, $message);

        return $this->redirectToRoute('admin_profile');
    }
}
