<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Repository\AdminRepository as AdminRep;
use App\Repository\IntervalRepository;
use App\Repository\PlanRepository;
use Doctrine\ORM\EntityManagerInterface as Manager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface as Mailer;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface as Trans;

/**
 * @Route("/admin", name="admin")
 * Class AdminController
 */
class AdminController extends AbstractController
{
    private AdminRep $repository;
    private Mailer $mailer;
    private Manager $manager;
    private Trans $translator;

    public function __construct(AdminRep $repository, Mailer $mailer, Manager $manager, Trans $translator)
    {
        $this->repository = $repository;
        $this->mailer = $mailer;
        $this->manager = $manager;
        $this->translator = $translator;
    }

    /**
     * @Route("/profile", name="_profile", methods={"GET"})
     */
    public function profile(PlanRepository $plan, IntervalRepository $interval)
    {
        /** @var User $user */
        $user = $this->getUser();
        $admin = $this->repository->findByUser($user->getId());
        $plans = $plan->findAll();
        $intervals = $interval->findAll();
        $return_data = ['admin' => $admin, 'plans' => $plans, 'intervals' => $intervals];

        return $this->render('admin/profile.html.twig', $return_data);
    }

    /**
     * @Route("/profile/edit", name="_profile_edit", methods={"GET"})
     */
    public function edit()
    {
        return $this->redirectToRoute('user_edit');
    }

    /**
     * @Route("/delete", name="_delete", methods={"DELETE"})
     */
    public function delete()
    {
        return $this->redirectToRoute('user_remove_unsubscribe');
    }
}
