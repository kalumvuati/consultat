<?php

namespace App\Entity;

use App\Repository\PaymentMethodRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @UniqueEntity(fields={"cardNumber"})
 * @ORM\Entity(repositoryClass=PaymentMethodRepository::class)
 */
class PaymentMethod
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\Regex(pattern="/\d/")
     * @Assert\Length(min=16, max=16)
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $cardNumber;

    /**
     * @Assert\GreaterThan(value="0")
     * @Assert\LessThanOrEqual(value="12")
     * @Assert\Regex(pattern="/\d/")
     * @Assert\Length(min=2, max=2)
     * @ORM\Column(type="integer", length=2, nullable=true)
     */
    private $cardMonth;

    /**
     * @Assert\GreaterThan(value="20")
     * @Assert\Regex(pattern="/\d/")
     * @Assert\Length(min=2, max=2)
     * @ORM\Column(type="integer", length=2, nullable=true)
     */
    private $cardYear;

    /**
     * @Assert\GreaterThan(value="0")
     * @Assert\Regex(pattern="/\d/")
     * @Assert\Length(min=3, max=3)
     * @ORM\Column(type="integer", length=3, nullable=true)
     */
    private $cardCvc;

    /**
     * @Assert\Regex(pattern="/([a-zA-Z]+( ){1,}([a-zA-Z]+)( ([a-zA-A]+))*)/")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cardNames;

    /**
     * @ORM\ManyToOne(targetEntity=Professional::class, inversedBy="paymentMethods")
     * @ORM\JoinColumn(nullable=true)
     */
    private $professional;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $stripeId;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActived = false;

    /**
     * @Gedmo\Slug(fields={"cardNumber"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @ORM\OneToOne(targetEntity=Client::class, mappedBy="paymentMethod", cascade={"persist", "remove"})
     */
    private $client;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return mixed
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param mixed $cardNumber
     *
     * @return PaymentMethod
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardMonth()
    {
        return $this->cardMonth;
    }

    /**
     * @param mixed $cardMonth
     *
     * @return PaymentMethod
     */
    public function setCardMonth($cardMonth)
    {
        $this->cardMonth = $cardMonth;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardYear()
    {
        return $this->cardYear;
    }

    /**
     * @param mixed $cardYear
     *
     * @return PaymentMethod
     */
    public function setCardYear($cardYear)
    {
        $this->cardYear = $cardYear;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardCvc()
    {
        return $this->cardCvc;
    }

    /**
     * @param mixed $cardCvc
     *
     * @return PaymentMethod
     */
    public function setCardCvc($cardCvc)
    {
        $this->cardCvc = $cardCvc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardNames()
    {
        return $this->cardNames;
    }

    /**
     * @param mixed $cardNames
     *
     * @return PaymentMethod
     */
    public function setCardNames($cardNames)
    {
        $this->cardNames = $cardNames;

        return $this;
    }

    public function getProfessional(): ?Professional
    {
        return $this->professional;
    }

    public function setProfessional(?Professional $professional): self
    {
        $this->professional = $professional;

        return $this;
    }

    public function getStripeId(): ?string
    {
        return $this->stripeId;
    }

    public function setStripeId(?string $stripeId): self
    {
        $this->stripeId = $stripeId;

        return $this;
    }

    public function getIsActived(): ?bool
    {
        return $this->isActived;
    }

    public function setIsActived(?bool $isActived): self
    {
        $this->isActived = $isActived;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        // unset the owning side of the relation if necessary
        if (null === $client && null !== $this->client) {
            $this->client->setPaymentMethod(null);
        }

        // set the owning side of the relation if necessary
        if (null !== $client && $client->getPaymentMethod() !== $this) {
            $client->setPaymentMethod($this);
        }

        $this->client = $client;

        return $this;
    }
}
