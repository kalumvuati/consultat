<?php

namespace App\Entity;

use App\Repository\PlanningRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PlanningRepository::class)
 */
class Planning
{
    public const DAYS = ['Monday' => '1', 'Tuesday' => '2', 'Wednesday' => '3', 'Thursday' => '4',
        'Friday' => '5', 'Saturday' => '6', 'Sunday' => '7', ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $day;

    /**
     * @ORM\ManyToOne(targetEntity=Professional::class, inversedBy="plannings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $professional;

    /**
     * @ORM\Column(type="time")
     */
    private $startPmAt;

    /**
     * @Assert\GreaterThan(propertyPath="startPmAt")
     * @ORM\Column(type="time")
     */
    private $endPmAt;

    /**
     * @Assert\GreaterThan(propertyPath="endPmAt")
     * @ORM\Column(type="time")
     */
    private $startAmAt;

    /**
     * @Assert\GreaterThan(propertyPath="startAmAt")
     * @ORM\Column(type="time")
     */
    private $endAmAt;

    /**
     * @Gedmo\Slug(fields={"day", "token"})
     * @ORM\Column(type="string", length="100", nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length="100")
     *
     * @var string
     */
    private $token;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __construct()
    {
        $this->token = sha1(uniqid('day', true));
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    public function getProfessional(): ?Professional
    {
        return $this->professional;
    }

    public function setProfessional(?Professional $professional): self
    {
        $this->professional = $professional;

        return $this;
    }

    public function getStartPmAt(): ?\DateTimeInterface
    {
        return $this->startPmAt;
    }

    public function setStartPmAt(\DateTimeInterface $startPmAt): self
    {
        $this->startPmAt = $startPmAt;

        return $this;
    }

    public function getEndPmAt(): ?\DateTimeInterface
    {
        return $this->endPmAt;
    }

    public function setEndPmAt(\DateTimeInterface $endPmAt): self
    {
        $this->endPmAt = $endPmAt;

        return $this;
    }

    public function getStartAmAt(): ?\DateTimeInterface
    {
        return $this->startAmAt;
    }

    public function setStartAmAt(\DateTimeInterface $startAmAt): self
    {
        $this->startAmAt = $startAmAt;

        return $this;
    }

    public function getEndAmAt(): ?\DateTimeInterface
    {
        return $this->endAmAt;
    }

    public function setEndAmAt(\DateTimeInterface $endAmAt): self
    {
        $this->endAmAt = $endAmAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @param mixed $day
     *
     * @return Planning
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    public function __toString()
    {
        return (string) array_search($this->day, self::DAYS, true);
    }
}
