<?php

namespace App\Entity;

use App\Repository\PlanRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @UniqueEntity(fields={"title"})
 * @ORM\Entity(repositoryClass=PlanRepository::class)
 */
class Plan
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $title;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $currency;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Interval::class, inversedBy="plan")
     * @ORM\JoinColumn(nullable=false)
     */
    private $intervale;

    /**
     * @ORM\OneToMany(targetEntity=Professional::class, mappedBy="plan")
     */
    private $Professionnals;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $subscriptionId;

    /**
     * @Gedmo\Slug(fields={"subscriptionId"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVisible;

    public function __construct()
    {
        $this->Professionnals = new ArrayCollection();
        $this->created_at = new \DateTime();
        $this->isVisible = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIntervale(): ?Interval
    {
        return $this->intervale;
    }

    public function setIntervale(?Interval $intervale): self
    {
        $this->intervale = $intervale;

        return $this;
    }

    /**
     * @return Collection|Professional[]
     */
    public function getProfessionnals(): Collection
    {
        return $this->Professionnals;
    }

    public function addProfessionnal(Professional $professionnal): self
    {
        if (!$this->Professionnals->contains($professionnal)) {
            $this->Professionnals[] = $professionnal;
            $professionnal->setPlan($this);
        }

        return $this;
    }

    public function removeProfessionnal(Professional $professionnal): self
    {
        if ($this->Professionnals->removeElement($professionnal)) {
            // set the owning side to null (unless already changed)
            if ($professionnal->getPlan() === $this) {
                $professionnal->setPlan(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getSubscriptionId(): ?string
    {
        return $this->subscriptionId;
    }

    public function setSubscriptionId(?string $subscriptionId): self
    {
        $this->subscriptionId = $subscriptionId;

        return $this;
    }

    public function __toString()
    {
        return $this->title;
    }

    public function getIsVisible(): ?bool
    {
        return $this->isVisible;
    }

    public function setIsVisible(bool $isVisible): self
    {
        $this->isVisible = $isVisible;

        return $this;
    }
}
