<?php

namespace App\Entity;

use App\Repository\ProfessionalRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ProfessionalRepository::class)
 */
class Professional extends Client
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @Assert\Length(min=14, max=14)
     * @ORM\Column(type="string", length=14)
     */
    private $siret;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $googleId;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActif;

    /**
     * @ORM\ManyToMany(targetEntity=Address::class, mappedBy="professionals")
     */
    private $addresses;

    /**
     * @ORM\OneToMany(targetEntity=Planning::class, mappedBy="professional", orphanRemoval=true)
     */
    private $plannings;

    /**
     * @ORM\OneToMany(targetEntity=Contact::class, mappedBy="professional", orphanRemoval=true)
     */
    private $contacts;

    /**
     * @ORM\ManyToMany(targetEntity=Domaine::class, mappedBy="professionals")
     */
    private $domaines;

    /**
     * @ORM\ManyToMany(targetEntity=Experience::class, mappedBy="professionals")
     */
    private $experiences;

    /**
     * @ORM\ManyToMany(targetEntity=Holiday::class, mappedBy="professionals")
     */
    private $holidays;

    /**
     * @ORM\ManyToMany(targetEntity=Diploma::class, mappedBy="professionals")
     */
    private $diplomas;

    /**
     * @ORM\ManyToOne(targetEntity=Plan::class, inversedBy="Professionnals")
     */
    private $plan;

    /**
     * @Gedmo\Slug(fields={"id", "siret"})
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $customerId;

    /**
     * @ORM\OneToMany(targetEntity=PaymentMethod::class, mappedBy="professional", orphanRemoval=true)
     */
    private $paymentMethods;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $subscriptionStripeId;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="professional", orphanRemoval=true)
     */
    private $events;

    public function __construct()
    {
        parent::__construct();
        $this->addresses = new ArrayCollection();
        $this->plannings = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        $this->domaines = new ArrayCollection();
        $this->experiences = new ArrayCollection();
        $this->holidays = new ArrayCollection();
        $this->diplomas = new ArrayCollection();
        $this->paymentMethods = new ArrayCollection();
        $this->events = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getGoogleId(): ?string
    {
        return $this->googleId;
    }

    public function setGoogleId(string $googleId): self
    {
        $this->googleId = $googleId;

        return $this;
    }

    public function getIsActif(): ?bool
    {
        return $this->isActif;
    }

    public function setIsActif(bool $isActif): self
    {
        $this->isActif = $isActif;

        return $this;
    }

    /**
     * @return Collection|Address[]
     */
    public function getAddresses(): Collection
    {
        return $this->addresses;
    }

    public function addAddress(Address $address): self
    {
        if (!$this->addresses->contains($address)) {
            $this->addresses[] = $address;
            $address->addProfessional($this);
        }

        return $this;
    }

    public function removeAddress(Address $address): self
    {
        if ($this->addresses->removeElement($address)) {
            $address->removeProfessional($this);
        }

        return $this;
    }

    /**
     * @return Collection|Planning[]
     */
    public function getPlannings(): Collection
    {
        return $this->plannings;
    }

    public function getPlanningsIntDayList()
    {
        $businessHours = [];
        /** @var Planning $planning */
        foreach ($this->plannings as $planning) {
            $start = $planning->getStartPmAt() ?? $planning->getStartAmAt();
            $end = $planning->getEndAmAt() ?? $planning->getEndPmAt();
            $businessHours[] = [
                'daysOfWeek' => $planning->getDay(), // Monday, Tuesday, Wednesday
                'startTime' => $planning->getStartPmAt()->format('H:i'), // 8am
                'endTime' => $planning->getEndPmAt()->format('H:i'), // 6pm
            ];
            $businessHours[] = [
                'daysOfWeek' => $planning->getDay(), // Monday, Tuesday, Wednesday
                'startTime' => $planning->getStartAmAt()->format('H:i'), // 8am
                'endTime' => $planning->getEndAmAt()->format('H:i'), // 6pm
            ];
        }

        return json_encode($businessHours);
    }

    public function addPlanning(Planning $planning): self
    {
        if (!$this->plannings->contains($planning)) {
            $this->plannings[] = $planning;
            $planning->setProfessional($this);
        }

        return $this;
    }

    public function removePlanning(Planning $planning): self
    {
        if ($this->plannings->removeElement($planning)) {
            // set the owning side to null (unless already changed)
            if ($planning->getProfessional() === $this) {
                $planning->setProfessional(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Contact[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contact $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setProfessional($this);
        }

        return $this;
    }

    public function removeContact(Contact $contact): self
    {
        if ($this->contacts->removeElement($contact)) {
            // set the owning side to null (unless already changed)
            if ($contact->getProfessional() === $this) {
                $contact->setProfessional(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Domaine[]
     */
    public function getDomaines(): Collection
    {
        return $this->domaines;
    }

    public function addDomaine(Domaine $domaine): self
    {
        if (!$this->domaines->contains($domaine)) {
            $this->domaines[] = $domaine;
            $domaine->addProfessional($this);
        }

        return $this;
    }

    public function removeDomaine(Domaine $domaine): self
    {
        if ($this->domaines->removeElement($domaine)) {
            $domaine->removeProfessional($this);
        }

        return $this;
    }

    /**
     * @return Collection|Experience[]
     */
    public function getExperiences(): Collection
    {
        return $this->experiences;
    }

    public function addExperience(Experience $experience): self
    {
        if (!$this->experiences->contains($experience)) {
            $this->experiences[] = $experience;
            $experience->addProfessional($this);
        }

        return $this;
    }

    public function removeExperience(Experience $experience): self
    {
        if ($this->experiences->removeElement($experience)) {
            $experience->removeProfessional($this);
        }

        return $this;
    }

    /**
     * @return Collection|Holiday[]
     */
    public function getHolidays(): Collection
    {
        return $this->holidays;
    }

    public function addHoliday(Holiday $holiday): self
    {
        if (!$this->holidays->contains($holiday)) {
            $this->holidays[] = $holiday;
            $holiday->addProfessional($this);
        }

        return $this;
    }

    public function removeHoliday(Holiday $holiday): self
    {
        if ($this->holidays->removeElement($holiday)) {
            $holiday->removeProfessional($this);
        }

        return $this;
    }

    /**
     * @return Collection|Diploma[]
     */
    public function getDiplomas(): Collection
    {
        return $this->diplomas;
    }

    public function addDiploma(Diploma $diploma): self
    {
        if (!$this->diplomas->contains($diploma)) {
            $this->diplomas[] = $diploma;
            $diploma->addProfessional($this);
        }

        return $this;
    }

    public function removeDiploma(Diploma $diploma): self
    {
        if ($this->diplomas->removeElement($diploma)) {
            $diploma->removeProfessional($this);
        }

        return $this;
    }

    public function getPlan(): ?Plan
    {
        return $this->plan;
    }

    public function setPlan(?Plan $plan): self
    {
        $this->plan = $plan;

        return $this;
    }

    public function getCustomerId(): ?string
    {
        return $this->customerId;
    }

    public function setCustomerId(?string $customerId): self
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * @return Collection|PaymentMethod[]
     */
    public function getPaymentMethods(): Collection
    {
        return $this->paymentMethods;
    }

    public function addPaymentMethod(PaymentMethod $paymentMethod): self
    {
        if (!$this->paymentMethods->contains($paymentMethod)) {
            $this->paymentMethods[] = $paymentMethod;
            $paymentMethod->setProfessional($this);
        }

        return $this;
    }

    public function removePaymentMethod(PaymentMethod $paymentMethod): self
    {
        if ($this->paymentMethods->removeElement($paymentMethod)) {
            // set the owning side to null (unless already changed)
            if ($paymentMethod->getProfessional() === $this) {
                $paymentMethod->setProfessional(null);
            }
        }

        return $this;
    }

    public function getSubscriptionStripeId(): ?string
    {
        return $this->subscriptionStripeId;
    }

    public function setSubscriptionStripeId(?string $subscriptionStripeId): self
    {
        $this->subscriptionStripeId = $subscriptionStripeId;

        return $this;
    }

    /**
     * @return ArrayCollection|Event[]
     */
    public function getEvents(): ArrayCollection
    {
        return $this->events ?? new ArrayCollection();
    }

    public function addEvent(Event $event): self
    {
        $this->events = $this->events ?? new ArrayCollection();
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setProfessional($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->removeElement($event)) {
            // set the owning side to null (unless already changed)
            if ($event->getProfessional() === $this) {
                $event->setProfessional(null);
            }
        }

        return $this;
    }
}
