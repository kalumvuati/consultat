<?php

namespace App\Entity;

use App\Repository\AddressRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AddressRepository::class)
 */
class Address
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $zipcode;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $complementProf;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $transportIndicationProf;

    /**
     * @ORM\OneToMany(targetEntity=Client::class, mappedBy="address")
     */
    private $clients;

    /**
     * @ORM\ManyToMany(targetEntity=Professional::class, inversedBy="addresses")
     */
    private $professionals;

    public function __construct()
    {
        $this->clients = new ArrayCollection();
        $this->professionals = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function setZipcode(string $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getComplementProf(): ?string
    {
        return $this->complementProf;
    }

    public function setComplementProf(?string $complementProf): self
    {
        $this->complementProf = $complementProf;

        return $this;
    }

    public function getTransportIndicationProf(): ?string
    {
        return $this->transportIndicationProf;
    }

    public function setTransportIndicationProf(?string $transportIndicationProf): self
    {
        $this->transportIndicationProf = $transportIndicationProf;

        return $this;
    }

    /**
     * @return Collection|Client[]
     */
    public function getClients(): Collection
    {
        return $this->clients;
    }

    public function addClient(Client $client): self
    {
        if (!$this->clients->contains($client)) {
            $this->clients[] = $client;
            $client->setAddress($this);
        }

        return $this;
    }

    public function removeClient(Client $client): self
    {
        if ($this->clients->removeElement($client)) {
            // set the owning side to null (unless already changed)
            if ($client->getAddress() === $this) {
                $client->setAddress(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Professional[]
     */
    public function getProfessionals(): Collection
    {
        return $this->professionals;
    }

    public function addProfessional(Professional $professional): self
    {
        if (!$this->professionals->contains($professional)) {
            $this->professionals[] = $professional;
        }

        return $this;
    }

    public function removeProfessional(Professional $professional): self
    {
        $this->professionals->removeElement($professional);

        return $this;
    }
}
