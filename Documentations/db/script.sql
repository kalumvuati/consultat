create table address
(
    id                        int auto_increment
        primary key,
    address                   varchar(255) not null,
    zipcode                   varchar(100) not null,
    city                      varchar(100) not null,
    complement_prof           varchar(255) null,
    transport_indication_prof varchar(255) null
)
    collate = utf8mb4_unicode_ci;

create table diploma
(
    id         int auto_increment
        primary key,
    title      varchar(100) not null,
    year       varchar(5)   not null,
    university varchar(100) not null,
    city       varchar(100) not null
)
    collate = utf8mb4_unicode_ci;

create table doctrine_migration_versions
(
    version        varchar(191) not null
        primary key,
    executed_at    datetime     null,
    execution_time int          null
)
    collate = utf8_unicode_ci;

create table domaine
(
    id          int auto_increment
        primary key,
    domaine     varchar(100) not null,
    description longtext     null,
    slug        varchar(100) null
)
    collate = utf8mb4_unicode_ci;

create table experience
(
    id          int auto_increment
        primary key,
    start_at    date         not null,
    end_at      date         not null,
    description longtext     null,
    location    varchar(255) not null,
    slug        varchar(255) not null,
    sluggable   varchar(255) not null
)
    collate = utf8mb4_unicode_ci;

create table holiday
(
    id          int auto_increment
        primary key,
    start_at    date     not null,
    end_at      date     not null,
    description longtext null
)
    collate = utf8mb4_unicode_ci;

create table `interval`
(
    id    int auto_increment
        primary key,
    title varchar(100) not null
)
    collate = utf8mb4_unicode_ci;

create table messenger_messages
(
    id           bigint auto_increment
        primary key,
    body         longtext     not null,
    headers      longtext     not null,
    queue_name   varchar(190) not null,
    created_at   datetime     not null,
    available_at datetime     not null,
    delivered_at datetime     null
)
    collate = utf8mb4_unicode_ci;

create index IDX_75EA56E016BA31DB
    on messenger_messages (delivered_at);

create index IDX_75EA56E0E3BD61CE
    on messenger_messages (available_at);

create index IDX_75EA56E0FB7336F0
    on messenger_messages (queue_name);

create table plan
(
    id              int auto_increment
        primary key,
    intervale_id    int          not null,
    title           varchar(100) not null,
    price           double       not null,
    currency        varchar(20)  not null,
    description     longtext     null,
    created_at      datetime     not null,
    subscription_id varchar(255) null,
    slug            varchar(255) null,
    is_visible      tinyint(1)   not null,
    constraint FK_DD5A5B7D4E85E29D
        foreign key (intervale_id) references `interval` (id)
)
    collate = utf8mb4_unicode_ci;

create index IDX_DD5A5B7D4E85E29D
    on plan (intervale_id);

create table user
(
    id          int auto_increment
        primary key,
    email       varchar(180) not null,
    roles       json         not null,
    password    varchar(255) not null,
    is_verified tinyint(1)   not null,
    created_at  datetime     not null,
    constraint UNIQ_8D93D649E7927C74
        unique (email)
)
    collate = utf8mb4_unicode_ci;

create table admin
(
    id      int auto_increment
        primary key,
    user_id int not null,
    constraint UNIQ_880E0D76A76ED395
        unique (user_id),
    constraint FK_880E0D76A76ED395
        foreign key (user_id) references user (id)
)
    collate = utf8mb4_unicode_ci;

create table client
(
    id                     int auto_increment
        primary key,
    user_id                int          not null,
    address_id             int          null,
    plan_id                int          null,
    name                   varchar(100) not null,
    lastname               varchar(100) not null,
    birtname               varchar(100) null,
    birthday               date         null,
    sex                    tinyint(1)   null,
    phone                  varchar(20)  not null,
    created_at             datetime     not null,
    discr                  varchar(255) not null,
    siret                  varchar(14)  null,
    google_id              varchar(255) null,
    is_actif               tinyint(1)   null,
    slug                   varchar(255) null,
    customer_id            varchar(255) null,
    subscription_stripe_id varchar(255) null,
    payment_method_id      int          null,
    constraint UNIQ_C7440455989D9B62
        unique (slug),
    constraint FK_C7440455A76ED395
        foreign key (user_id) references user (id),
    constraint FK_C7440455E899029B
        foreign key (plan_id) references plan (id),
    constraint FK_C7440455F5B7AF75
        foreign key (address_id) references address (id)
)
    collate = utf8mb4_unicode_ci;

create table address_professional
(
    address_id      int not null,
    professional_id int not null,
    primary key (address_id, professional_id),
    constraint FK_BF14A7B1DB77003
        foreign key (professional_id) references client (id)
            on delete cascade,
    constraint FK_BF14A7B1F5B7AF75
        foreign key (address_id) references address (id)
            on delete cascade
)
    collate = utf8mb4_unicode_ci;

create index IDX_BF14A7B1DB77003
    on address_professional (professional_id);

create index IDX_BF14A7B1F5B7AF75
    on address_professional (address_id);

create index IDX_C74404555AA1164F
    on client (payment_method_id);

create index IDX_C7440455A76ED395
    on client (user_id);

create index IDX_C7440455E899029B
    on client (plan_id);

create index IDX_C7440455F5B7AF75
    on client (address_id);

create table contact
(
    id              int auto_increment
        primary key,
    professional_id int          not null,
    email           varchar(255) not null,
    site            varchar(255) null,
    mobile          varchar(20)  null,
    fixe            varchar(20)  null,
    fax             varchar(20)  null,
    slug            varchar(255) null,
    constraint FK_4C62E638DB77003
        foreign key (professional_id) references client (id)
)
    collate = utf8mb4_unicode_ci;

create index IDX_4C62E638DB77003
    on contact (professional_id);

create table diploma_professional
(
    diploma_id      int not null,
    professional_id int not null,
    primary key (diploma_id, professional_id),
    constraint FK_AB5C8B3A99ACEB5
        foreign key (diploma_id) references diploma (id)
            on delete cascade,
    constraint FK_AB5C8B3DB77003
        foreign key (professional_id) references client (id)
            on delete cascade
)
    collate = utf8mb4_unicode_ci;

create index IDX_AB5C8B3A99ACEB5
    on diploma_professional (diploma_id);

create index IDX_AB5C8B3DB77003
    on diploma_professional (professional_id);

create table domaine_professional
(
    domaine_id      int not null,
    professional_id int not null,
    primary key (domaine_id, professional_id),
    constraint FK_39F7FC74272FC9F
        foreign key (domaine_id) references domaine (id)
            on delete cascade,
    constraint FK_39F7FC7DB77003
        foreign key (professional_id) references client (id)
            on delete cascade
)
    collate = utf8mb4_unicode_ci;

create index IDX_39F7FC74272FC9F
    on domaine_professional (domaine_id);

create index IDX_39F7FC7DB77003
    on domaine_professional (professional_id);

create table event
(
    id              int auto_increment
        primary key,
    client_id       int          null,
    title           varchar(255) not null,
    start_at        datetime     not null,
    end_at          datetime     not null,
    is_aborted      tinyint(1)   not null,
    description     longtext     null,
    professional_id int          null,
    reason          longtext     null,
    validated_at    datetime     null,
    is_free         tinyint(1)   not null,
    modified_at     datetime     null,
    constraint FK_3BAE0AA719EB6921
        foreign key (client_id) references client (id),
    constraint FK_3BAE0AA7DB77003
        foreign key (professional_id) references client (id)
)
    collate = utf8mb4_unicode_ci;

create index IDX_3BAE0AA719EB6921
    on event (client_id);

create index IDX_3BAE0AA7DB77003
    on event (professional_id);

create table experience_professional
(
    experience_id   int not null,
    professional_id int not null,
    primary key (experience_id, professional_id),
    constraint FK_A01BD3A246E90E27
        foreign key (experience_id) references experience (id)
            on delete cascade,
    constraint FK_A01BD3A2DB77003
        foreign key (professional_id) references client (id)
            on delete cascade
)
    collate = utf8mb4_unicode_ci;

create index IDX_A01BD3A246E90E27
    on experience_professional (experience_id);

create index IDX_A01BD3A2DB77003
    on experience_professional (professional_id);

create table holiday_professional
(
    holiday_id      int not null,
    professional_id int not null,
    primary key (holiday_id, professional_id),
    constraint FK_C86E97D6830A3EC0
        foreign key (holiday_id) references holiday (id)
            on delete cascade,
    constraint FK_C86E97D6DB77003
        foreign key (professional_id) references client (id)
            on delete cascade
)
    collate = utf8mb4_unicode_ci;

create index IDX_C86E97D6830A3EC0
    on holiday_professional (holiday_id);

create index IDX_C86E97D6DB77003
    on holiday_professional (professional_id);

create table payment_method
(
    id              int auto_increment
        primary key,
    professional_id int          null,
    card_number     varchar(20)  null,
    card_month      int          null,
    card_year       int          null,
    card_cvc        int          null,
    card_names      varchar(255) null,
    stripe_id       varchar(255) null,
    is_actived      tinyint(1)   not null,
    slug            varchar(255) null,
    constraint FK_7B61A1F6DB77003
        foreign key (professional_id) references client (id)
)
    collate = utf8mb4_unicode_ci;

alter table client
    add constraint FK_C74404555AA1164F
        foreign key (payment_method_id) references payment_method (id);

create index IDX_7B61A1F6DB77003
    on payment_method (professional_id);

create table planning
(
    id              int auto_increment
        primary key,
    professional_id int          not null,
    day             varchar(20)  not null,
    start_pm_at     time         not null,
    end_pm_at       time         not null,
    start_am_at     time         not null,
    end_am_at       time         not null,
    slug            varchar(100) null,
    token           varchar(100) not null,
    constraint FK_D499BFF6DB77003
        foreign key (professional_id) references client (id)
)
    collate = utf8mb4_unicode_ci;

create index IDX_D499BFF6DB77003
    on planning (professional_id);


