<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210617151033 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL, address VARCHAR(255) NOT NULL, zipcode VARCHAR(100) NOT NULL, city VARCHAR(100) NOT NULL, complement_prof VARCHAR(255) DEFAULT NULL, transport_indication_prof VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE address_professional (address_id INT NOT NULL, professional_id INT NOT NULL, INDEX IDX_BF14A7B1F5B7AF75 (address_id), INDEX IDX_BF14A7B1DB77003 (professional_id), PRIMARY KEY(address_id, professional_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `admin` (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, UNIQUE INDEX UNIQ_880E0D76A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, address_id INT DEFAULT NULL, payment_method_id INT DEFAULT NULL, plan_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, lastname VARCHAR(100) NOT NULL, birthday DATE DEFAULT NULL, sex TINYINT(1) DEFAULT NULL, phone VARCHAR(20) DEFAULT NULL, created_at DATETIME NOT NULL, google_sub VARCHAR(255) DEFAULT NULL, discr VARCHAR(255) NOT NULL, siret VARCHAR(14) DEFAULT NULL, google_id VARCHAR(255) DEFAULT NULL, is_actif TINYINT(1) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, customer_id VARCHAR(255) DEFAULT NULL, subscription_stripe_id VARCHAR(255) DEFAULT NULL, INDEX IDX_C7440455A76ED395 (user_id), INDEX IDX_C7440455F5B7AF75 (address_id), INDEX IDX_C74404555AA1164F (payment_method_id), UNIQUE INDEX UNIQ_C7440455989D9B62 (slug), INDEX IDX_C7440455E899029B (plan_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact (id INT AUTO_INCREMENT NOT NULL, professional_id INT NOT NULL, email VARCHAR(255) NOT NULL, site VARCHAR(255) DEFAULT NULL, mobile VARCHAR(20) DEFAULT NULL, fixe VARCHAR(20) DEFAULT NULL, fax VARCHAR(20) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, INDEX IDX_4C62E638DB77003 (professional_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE diploma (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(100) NOT NULL, year VARCHAR(5) NOT NULL, university VARCHAR(100) NOT NULL, city VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE diploma_professional (diploma_id INT NOT NULL, professional_id INT NOT NULL, INDEX IDX_AB5C8B3A99ACEB5 (diploma_id), INDEX IDX_AB5C8B3DB77003 (professional_id), PRIMARY KEY(diploma_id, professional_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE domaine (id INT AUTO_INCREMENT NOT NULL, domaine VARCHAR(100) NOT NULL, description LONGTEXT DEFAULT NULL, slug VARCHAR(100) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE domaine_professional (domaine_id INT NOT NULL, professional_id INT NOT NULL, INDEX IDX_39F7FC74272FC9F (domaine_id), INDEX IDX_39F7FC7DB77003 (professional_id), PRIMARY KEY(domaine_id, professional_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, professional_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, start_at DATETIME NOT NULL, end_at DATETIME NOT NULL, is_aborted TINYINT(1) NOT NULL, description LONGTEXT DEFAULT NULL, reason LONGTEXT DEFAULT NULL, validated_at DATETIME DEFAULT NULL, is_free TINYINT(1) NOT NULL, modified_at DATETIME DEFAULT NULL, INDEX IDX_3BAE0AA719EB6921 (client_id), INDEX IDX_3BAE0AA7DB77003 (professional_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE experience (id INT AUTO_INCREMENT NOT NULL, start_at DATE NOT NULL, end_at DATE NOT NULL, description LONGTEXT DEFAULT NULL, location VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, sluggable VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE experience_professional (experience_id INT NOT NULL, professional_id INT NOT NULL, INDEX IDX_A01BD3A246E90E27 (experience_id), INDEX IDX_A01BD3A2DB77003 (professional_id), PRIMARY KEY(experience_id, professional_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE holiday (id INT AUTO_INCREMENT NOT NULL, start_at DATE NOT NULL, end_at DATE NOT NULL, description LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE holiday_professional (holiday_id INT NOT NULL, professional_id INT NOT NULL, INDEX IDX_C86E97D6830A3EC0 (holiday_id), INDEX IDX_C86E97D6DB77003 (professional_id), PRIMARY KEY(holiday_id, professional_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `interval` (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE payment_method (id INT AUTO_INCREMENT NOT NULL, professional_id INT DEFAULT NULL, card_number VARCHAR(16) DEFAULT NULL, card_month INT DEFAULT NULL, card_year INT DEFAULT NULL, card_cvc INT DEFAULT NULL, card_names VARCHAR(255) DEFAULT NULL, stripe_id VARCHAR(255) DEFAULT NULL, is_actived TINYINT(1) NOT NULL, slug VARCHAR(255) DEFAULT NULL, INDEX IDX_7B61A1F6DB77003 (professional_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE plan (id INT AUTO_INCREMENT NOT NULL, intervale_id INT NOT NULL, title VARCHAR(100) NOT NULL, price DOUBLE PRECISION NOT NULL, currency VARCHAR(20) NOT NULL, description LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, subscription_id VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, is_visible TINYINT(1) NOT NULL, INDEX IDX_DD5A5B7D4E85E29D (intervale_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE planning (id INT AUTO_INCREMENT NOT NULL, professional_id INT NOT NULL, day VARCHAR(20) NOT NULL, start_pm_at TIME NOT NULL, end_pm_at TIME NOT NULL, start_am_at TIME NOT NULL, end_am_at TIME NOT NULL, slug VARCHAR(100) DEFAULT NULL, token VARCHAR(100) NOT NULL, INDEX IDX_D499BFF6DB77003 (professional_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) DEFAULT NULL, is_verified TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE address_professional ADD CONSTRAINT FK_BF14A7B1F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE address_professional ADD CONSTRAINT FK_BF14A7B1DB77003 FOREIGN KEY (professional_id) REFERENCES client (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE `admin` ADD CONSTRAINT FK_880E0D76A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C7440455A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C7440455F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C74404555AA1164F FOREIGN KEY (payment_method_id) REFERENCES payment_method (id)');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C7440455E899029B FOREIGN KEY (plan_id) REFERENCES plan (id)');
        $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E638DB77003 FOREIGN KEY (professional_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE diploma_professional ADD CONSTRAINT FK_AB5C8B3A99ACEB5 FOREIGN KEY (diploma_id) REFERENCES diploma (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE diploma_professional ADD CONSTRAINT FK_AB5C8B3DB77003 FOREIGN KEY (professional_id) REFERENCES client (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE domaine_professional ADD CONSTRAINT FK_39F7FC74272FC9F FOREIGN KEY (domaine_id) REFERENCES domaine (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE domaine_professional ADD CONSTRAINT FK_39F7FC7DB77003 FOREIGN KEY (professional_id) REFERENCES client (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA719EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA7DB77003 FOREIGN KEY (professional_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE experience_professional ADD CONSTRAINT FK_A01BD3A246E90E27 FOREIGN KEY (experience_id) REFERENCES experience (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE experience_professional ADD CONSTRAINT FK_A01BD3A2DB77003 FOREIGN KEY (professional_id) REFERENCES client (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE holiday_professional ADD CONSTRAINT FK_C86E97D6830A3EC0 FOREIGN KEY (holiday_id) REFERENCES holiday (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE holiday_professional ADD CONSTRAINT FK_C86E97D6DB77003 FOREIGN KEY (professional_id) REFERENCES client (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE payment_method ADD CONSTRAINT FK_7B61A1F6DB77003 FOREIGN KEY (professional_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE plan ADD CONSTRAINT FK_DD5A5B7D4E85E29D FOREIGN KEY (intervale_id) REFERENCES `interval` (id)');
        $this->addSql('ALTER TABLE planning ADD CONSTRAINT FK_D499BFF6DB77003 FOREIGN KEY (professional_id) REFERENCES client (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE address_professional DROP FOREIGN KEY FK_BF14A7B1F5B7AF75');
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C7440455F5B7AF75');
        $this->addSql('ALTER TABLE address_professional DROP FOREIGN KEY FK_BF14A7B1DB77003');
        $this->addSql('ALTER TABLE contact DROP FOREIGN KEY FK_4C62E638DB77003');
        $this->addSql('ALTER TABLE diploma_professional DROP FOREIGN KEY FK_AB5C8B3DB77003');
        $this->addSql('ALTER TABLE domaine_professional DROP FOREIGN KEY FK_39F7FC7DB77003');
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA719EB6921');
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA7DB77003');
        $this->addSql('ALTER TABLE experience_professional DROP FOREIGN KEY FK_A01BD3A2DB77003');
        $this->addSql('ALTER TABLE holiday_professional DROP FOREIGN KEY FK_C86E97D6DB77003');
        $this->addSql('ALTER TABLE payment_method DROP FOREIGN KEY FK_7B61A1F6DB77003');
        $this->addSql('ALTER TABLE planning DROP FOREIGN KEY FK_D499BFF6DB77003');
        $this->addSql('ALTER TABLE diploma_professional DROP FOREIGN KEY FK_AB5C8B3A99ACEB5');
        $this->addSql('ALTER TABLE domaine_professional DROP FOREIGN KEY FK_39F7FC74272FC9F');
        $this->addSql('ALTER TABLE experience_professional DROP FOREIGN KEY FK_A01BD3A246E90E27');
        $this->addSql('ALTER TABLE holiday_professional DROP FOREIGN KEY FK_C86E97D6830A3EC0');
        $this->addSql('ALTER TABLE plan DROP FOREIGN KEY FK_DD5A5B7D4E85E29D');
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C74404555AA1164F');
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C7440455E899029B');
        $this->addSql('ALTER TABLE `admin` DROP FOREIGN KEY FK_880E0D76A76ED395');
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C7440455A76ED395');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE address_professional');
        $this->addSql('DROP TABLE `admin`');
        $this->addSql('DROP TABLE client');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE diploma');
        $this->addSql('DROP TABLE diploma_professional');
        $this->addSql('DROP TABLE domaine');
        $this->addSql('DROP TABLE domaine_professional');
        $this->addSql('DROP TABLE event');
        $this->addSql('DROP TABLE experience');
        $this->addSql('DROP TABLE experience_professional');
        $this->addSql('DROP TABLE holiday');
        $this->addSql('DROP TABLE holiday_professional');
        $this->addSql('DROP TABLE `interval`');
        $this->addSql('DROP TABLE payment_method');
        $this->addSql('DROP TABLE plan');
        $this->addSql('DROP TABLE planning');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
