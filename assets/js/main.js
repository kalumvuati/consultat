checkSiret();

function checkSiret()
{
    let selector_v = document.querySelector("#siret_checker_js");
    if (selector_v) {
        selector_v.addEventListener('submit', e => {
            e.preventDefault();
            let siret_field = $("#siret");
            let url = selector_v.action;
            let siret_return_message = document.querySelector('#siret_return_message_js');
            let register_professinal_js = document.querySelector("#register_professional_js");
            $.ajax({
                url: url,
                type:"POST",
                data: {siret: siret_field.val()},
                cache: false,
                success: function (response) {
                    if (response.status === 200) {
                        if (register_professinal_js) {
                            //For title
                            let prof_siret_denomination = document.querySelector("#prof_siret_denomination");
                            prof_siret_denomination.textContent = response.denomination

                            //For input field
                            let prof_siret_siret = document.querySelector("#siret_js");
                            prof_siret_siret.value = response.siret
                            console.log(siret_return_message.classList);
                            removeAlert('#siret_return_message_js');
                            siret_return_message.classList.add('alert-success');
                            siret_return_message.textContent = response.message;
                            siret_return_message.style.display = 'block';
                            register_professinal_js.style.display ="block";
                        }
                    } else{
                        // handle error
                        siret_return_message.textContent = response.message;
                        removeAlert('#siret_return_message_js');
                        siret_return_message.classList.add('alert-danger')
                        siret_return_message.style.display = 'block';
                    }

                },
                error: function (error) {
                    if (error.status === 404) {
                        siret_return_message.textContent = message
                        siret_return_message.style.display = 'block';
                        removeAlert('#siret_return_message_js')
                        siret_return_message.classList.add('alert-danger')
                        let register_professinal_js = document.querySelector("#register_professional_js");
                        if (register_professinal_js) {
                            register_professinal_js.style.display ="none";
                        }
                    }
                }
            });
        });
    }
}

function removeAlert(selector)
{
    let siret_return_message = document.querySelector(selector)
    if (siret_return_message) {
        if (siret_return_message.classList.contains('alert-danger')) {
            siret_return_message.classList.remove('alert-danger');
        }
        if (siret_return_message.classList.contains('alert-success')) {
            siret_return_message.classList.remove('alert-success');
        }
        siret_return_message.style.display ='none';
    }
}