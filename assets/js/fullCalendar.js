import {Calendar, globalLocales} from '@fullcalendar/core';
import adaptivePlugin from '@fullcalendar/adaptive';
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import listPlugin from '@fullcalendar/list';
import timeGridPlugin from '@fullcalendar/timegrid';
import frLocale from '@fullcalendar/core/locales/fr';
import axios from "axios";
import modal from 'bootstrap/js/dist/modal'
// import resourceTimelinePlugin from '@fullcalendar/resource-timeline';
let event_delete_js = $('#event_delete_js');
let event_delete_btn_js = $('#event_delete_btn_js');

fullCalendarInit();

function showEvent(event, isEdit = false) {
    //Bootstrap Modal - instance
    let appointment = document.querySelector("#appointment");
    if (appointment) {
        let url = "";
        if (isEdit) {
            url = "/professional/event/" + event.id + "/edit";
            eventForm.description.val(event.extendedProps.description);
            eventForm.title.val(event.title);
            event_delete_btn_js.click( e =>{
                url = "/professional/event/delete/" + event.id;
                let method_field = $('.modal-footer');
                let input = document.createElement('input');
                input.name ="_method";
                input.value="delete";
                input.type ="hidden";
                method_field.append(input);
                eventForm.formula.attr('action', url);
                //change Action attribute
            });
        }else {
            url = "/professional/event/new";
            event_delete_btn_js.remove()
        }
        eventForm.formula.attr('action', url);
        let myBodal = new modal(appointment, {
            keyboard: false
        });
        //Setting Info on modal element
        eventForm.start_date.val(getDate(event.start));
        eventForm.start_time.val(getTime(event.start));

        eventForm.end_date.val(getDate(event.end));
        eventForm.end_time.val(getTime(event.end));
         console.log(eventForm.formula)
        myBodal.show();

    }

}

/**
 *
 * @param events : events from database
 * @param solo : bolean for printing calendar without btns on top and less details
 */
function fullCalendarInit() {
    let calendarEl = document.querySelector("#fullCalendar_js");
    let fullCalendar = null;
    if (calendarEl) {
        let input_events_js = document.querySelector('#events_js');
        let planning = calendarEl.dataset.planning?JSON.parse(calendarEl.dataset.planning):[];
        let events = input_events_js?JSON.parse(input_events_js.value):[]
        let url = calendarEl.dataset.url??"";
        //Url determines user or professional
        if (url) {
            //Client
            fullCalendar = new Calendar(calendarEl,{
                initialView:'timeGridWeek',
                plugins: [ dayGridPlugin, timeGridPlugin, listPlugin, interactionPlugin],
                locale: frLocale,
                timeZone: 'locale',
                nowIndicator: true,
                //selectable: true,
                editable: true,
                businessHours: planning,
                headerToolbar: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth,timeGridWeek,listDay'
                },
                events: events,
                eventClick: function (event) {
                    console.log("client click on event : ", event)
                    showEvent(event.event);
                }
            });
        }else
        {
            //Professional
            fullCalendar = new Calendar(calendarEl,{
                initialView:'timeGridWeek',
                plugins: [ dayGridPlugin, timeGridPlugin, listPlugin, interactionPlugin],
                locale: frLocale,
                timeZone: 'locale',
                nowIndicator: true,
                selectable: true,
                businessHours: planning,
                headerToolbar: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth,timeGridWeek,listDay'
                },
                events: events,
                editable: true,
                eventResizableFromStart:true,
                select: function (event) {
                    console.log("Professional select :", event)
                    showEvent(event);
                },
                eventClick: function (event) {
                    console.log("Professional clicked :", event.event, event.event.id)
                    showEvent(event.event, true);
                },
                eventChange: function (event) {
                    updateAjax(event.event);
                }
            });
        }
        fullCalendar.render();
    }
}

/**
 * @param professionalId
 */
function getProfessionalPlanning(professionalId)
{
    let url = "/professional/event/" + professionalId;
    axios.post(url, {})
        .then(function (response) {
            if (response.status === 200) {
                console.log("OK ", response.data);
            } else{
                console.log("OK with ERROR ", response.data);
            }
        })
        .catch(function (error) {
            // handle error
            console.error("FATAL ERROR", error.message);
        });
}

/**
 * Trait date sent by fullCalendar event
 * @param js_date
 * @param separate
 * @returns {*} return the formated date according my will
 */
function getDate(js_date,separate ="-") {
    let day     = js_date.getDate() < 10 ? "0" + js_date.getDate(): js_date.getDate();
    let month   = (js_date.getMonth() + 1) < 10? "0"+(js_date.getMonth() + 1):(js_date.getMonth() + 1);
    let year    = js_date.getFullYear();
    return `${year}-${month}-${day}`;
}

/**
 * Trait time received from fullCalendar event
 *
 * @param js_date
 * @returns {string}
 */
function getTime(js_date) {
    let hour    = js_date.getUTCHours() < 10 ? "0" + js_date.getUTCHours():js_date.getUTCHours();
    let min     = js_date.getUTCMinutes() < 10 ? "0" + js_date.getUTCMinutes():js_date.getUTCMinutes();
    let sec     = js_date.getUTCSeconds() < 10 ? "0"+js_date.getUTCSeconds():js_date.getUTCSeconds();
    return `${hour}:${min}:${sec}`;
}

/**
 * cleans all fields form of Event Form
 */
function cleanEventFields() {
    eventForm.id.val('');
    eventForm.title.val('');
    eventForm.start_date.val('');
    eventForm.start_time.val('');
    eventForm.end_date.val('');
    eventForm.end_time.val('');
    eventForm.description.val('');
    eventForm.location.val('');
}

function updateAjax(fullCalendarEvent)
{
    if (fullCalendarEvent) {
        let ajax_data   = fullCalendarEvent.toJSON();
        let url = `/professional/event/${fullCalendarEvent.id}/editJson`;
        axios.post(url, ajax_data)
            .then(function (response) {
                if (response.status === 202) {
                    console.log(response.data)
                    //For title
                } else{
                    // handle error
                    console.error(response.data)
                }
            })
            .catch(function (error) {
                // handle error
                console.error(error.message);
            });
    }
}

function createEvent(fullCalendarEvent)
{
    if (fullCalendarEvent) {
        let ajax_data = {
            id: 0,
            title: eventForm.title.val(),
            start: `${eventForm.start_date.val()} ${eventForm.start_time.val()}` ,
            end: `${eventForm.end_date.val()} ${eventForm.end_time.val()}`,
            // backgroundColor: eventForm.backgroundColor.val(),
            extendedProps: {
                description: eventForm.description.val(),
            },
        }
        //Professional_slug example -> 88254332500014
        let $professional_slug = document.querySelector("#professional_slug");
        let url = "/client/event/appointment/" + $professional_slug;
        axios.post(url, ajax_data)
            .then(function (response) {
                if (response.status === 200) {
                    console.log(response.data)
                    //For title
                } else{
                    // handle error
                    console.error(response.data)
                }
            })
            .catch(function (error) {
                // handle error
                console.error(error.message);
            });
    }
}

const eventForm = {
    id: $('#id'),
    title: $('#title'),
    start_date: $('#startAt'),
    start_time: $('#startAtTime'),
    end_date: $('#endAt'),
    end_time: $('#endAtTime'),
    description: $('#description'),
    backgroundColor: $('#backgroundColor'),
    location: $('#location'),
    formula: $('#form_js'),
}